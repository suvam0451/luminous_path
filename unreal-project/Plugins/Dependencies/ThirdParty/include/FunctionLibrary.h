#pragma once  

#define WINTERWILDFIRE_API __declspec(dllexport)	//shortens __declspec(dllexport) to DLL_EXPORT
#define loop(i,N) for(int i = 0; i < N; i++)

#ifdef __cplusplus		//if C++ is used convert it to C to prevent C++'s name mangling of method names

extern "C++"
{
#endif
	enum WINTERWILDFIRE_API States {
		Walking,
		Grabbing,
		Running,
		Flying,

		Jumping,
		Falling,
		Hanging,
		Grounded,
		Floating,
		Dashing
	};
	enum WINTERWILDFIRE_API Responses {
		DoNothing,
		Sprint,
		Jump,
		StartDashing,
		DashNotAvailable
	};

	struct WINTERWILDFIRE_API Vector3 {
		float x;
		float y;
		float z;
	};
	bool WINTERWILDFIRE_API WWdashAvailable = false;
	bool WINTERWILDFIRE_API WWOnGround = false;
	bool WINTERWILDFIRE_API WWCanMoveMoveAhead = true;
	bool WINTERWILDFIRE_API WWCanMoveSideways = true;
	bool WINTERWILDFIRE_API references[4];
	//bool* DLL_EXPORT dragon[4];

	int WINTERWILDFIRE_API sendStateChangeRequest(int initial, int final);
	void WINTERWILDFIRE_API sendStateUpdate(int* array, int N);
	void WINTERWILDFIRE_API InitializeDLL();

	void WINTERWILDFIRE_API returnVectorList(Vector3 in, Vector3* &ass);

	int WINTERWILDFIRE_API testFunction(int myarray);
#ifdef __cplusplus


}
#endif