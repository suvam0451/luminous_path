
#include "WW_Library.h"

#pragma region Regex Helpers

/*  Static helper methods for Regex Queries
*	Look into WW_Library.h for definitions 
*/

bool WW_RegexHelper::SimpleRegexQuery(FString In, FString regex) {
	FRegexPattern myPattern(*regex);
	FRegexMatcher myMatcher(myPattern, *In);
	return myMatcher.FindNext() ? true : false;
}

FString WW_RegexHelper::GetAssetName(FString In)
{
	return FString();
}

FString WW_RegexHelper::GetAssetName(FString In, FString regex) {
	TPair<int32, int32> indices;
	FString retval;

	// This gets rid of the type portion "SM_"
	if (SimpleRegexQuery(In, regex, indices)) {
		In = In.RightChop(indices.Value);
	}

	// This part gets the asset name
	if (SimpleRegexQuery(In, "^(.*?)+(_|-)", indices)) {
		In = In.LeftChop(In.Len() - indices.Value + 1);
	}
	else if (SimpleRegexQuery(In, "^(.*?)+$", indices)) {
		In = In.LeftChop(In.Len() - indices.Value);
	}
	return In;
}

FString WW_RegexHelper::RegexExtractDirectory(FString pathname, FString filename) {
	FRegexPattern myPattern(*filename);
	FRegexMatcher myMatcher(myPattern, *pathname);
	return (myMatcher.FindNext()) ? pathname.LeftChop(pathname.Len() - myMatcher.GetMatchBeginning() + 1) : "";
}



bool WW_RegexHelper::SimpleRegexQuery(FString In, FString regex, TPair<int32, int32>& retval) {
	FRegexPattern myPattern(*regex);
	FRegexMatcher myMatcher(myPattern, *In);
	if (myMatcher.FindNext()) {
		retval.Key = myMatcher.GetMatchBeginning();
		retval.Value = myMatcher.GetMatchEnding();
		return true;
	}
	return false;
}

#pragma endregion