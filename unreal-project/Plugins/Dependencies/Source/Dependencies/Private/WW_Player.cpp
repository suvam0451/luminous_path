// Fill out your copyright notice in the Description page of Project Settings.

#include "WW_Player.h"
//#include "CustomUI/Public/CameraShake01.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
//#include "Dependencies/Public/Libraries/StaticLib.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "AbilitySystemComponent.h"
#include "WW_SP_PlayerState.h"
#include "WW_Library.h"
#include "WW_Interfaces.h"
#include <Runtime\Engine\Public\DrawDebugHelpers.h>

// Sets default values
AWW_Player::AWW_Player()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	_camera = CreateDefaultSubobject<UCameraComponent>(TEXT("camera"));
	_camera->SetupAttachment(GetCapsuleComponent());
	_camera->bUsePawnControlRotation = false;

	PlayerMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	PlayerMesh->SetOnlyOwnerSee(true);
	PlayerMesh->SetupAttachment(_camera);
	PlayerMesh->bCastDynamicShadow = false;
	PlayerMesh->CastShadow = false;
	PlayerMesh->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	PlayerMesh->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);

	AbilitySystemComponent = CreateDefaultSubobject<UWW_AbilitySysComp>(TEXT("AbilitySystemComponent"));
	AbilitySystemComponent->SetIsReplicated(true);

	AttributeSet = CreateDefaultSubobject<UWW_Attributes>(TEXT("Attribute Set"));

	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
}

// Returns our ability system component.
UAbilitySystemComponent* AWW_Player::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

void AWW_Player::OnRep_PlayerState()
{
	Super::OnRep_PlayerState();
	this->UpdateAbilityStack();
}

void AWW_Player::UpdateAbilityStack()
{
	// Replace with your PlayerState here... **Default is APlayerState**
	//APlayerState* _playerState = Cast<APlayerState>(this->GetPlayerState());
	//if (_playerState && IsValid(_playerState))
	//{
	//	AbilitySystemComponent = Cast<UWW_AbilitySysComp>(_playerState->GetAbilitySystemComponent());
	//	AttributeSet = Cast<UWW_Attributes>(_playerState->GetAttributeSet());
	//	
	//	AbilitySystemComponent->InitAbilityActorInfo(this->GetPlayerState(), this);
	//}
}

// Called when the game starts or when spawned
void AWW_Player::BeginPlay()
{
	Super::BeginPlay();
	//StateManagerRef = 
	StateManagerRef = NewObject<UHuman_StateManager>();
	StateManagerRef->VerticalState = EPlayerStates::Grounded;
	StateManagerRef->HorizontalState = EPlayerStates::Walking;
	StateManagerRef->bOnGround = true;
	StateManagerRef->PlayerRef = this;
	

	//StateManagerRef
	horizontalState = EPlayerStates::Walking;
	verticalState = EPlayerStates::Grounded;

	// Gets a reference got all uses in class
	ControllerRef = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	//One liner if you want to do in current scope
	APlayerController* example = UGameplayStatics::GetPlayerController(GetWorld(), 0);
}

void AWW_Player::OnConstruction(const FTransform& Transform)
{
	GetCapsuleComponent()->InitCapsuleSize(CapsuleRadius, CapsuleHeight);
	GetCapsuleComponent()->SetCapsuleSize(CapsuleRadius, CapsuleHeight);

	//GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	//GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled:);

	_camera->RelativeLocation = CameraRelativeLocation; // Position the camera
}

// Called every frame
void AWW_Player::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector Loc;
	FRotator Rot;
	FHitResult Hit;

	GetController()->GetPlayerViewPoint(Loc, Rot);
	
	FVector Start = Loc;
	FVector End = Start + Rot.Vector() * 200.0f;
	FCollisionQueryParams TraceParams;



	// Handle interactibles...
	bool bHit = GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_Visibility, TraceParams);
	// DrawDebugLine(GetWorld(), Start, End, FColor::Orange, false, 2.0f);
	if (bHit) {
		AActor* Interactable = Hit.GetActor();
		IWW_Pickup* _interface = Cast<IWW_Pickup>(Interactable);
		_interface ? _interface->Execute_StartFocus(Interactable) : true;
	}
}

void AWW_Player::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	//UpdateAbilityStack();
}

template<EPlayerActions actionName, bool isPressed>
void AWW_Player::ActionMapper()
{
	//---Mapping key presses to functions after DLL query...
	switch (actionName) {
	case EPlayerActions::Sprint: { isPressed ? ReactionMapper(EResponses::StartSprint) : ReactionMapper(EResponses::StopSprint); break; }
	case EPlayerActions::Jump: { isPressed ? StateManagerRef->TRequest_Vert(EPlayerStates::Jumping) : true; break; }
	default: { break; }
	}
}

template<int32 axisName>
void AWW_Player::AxisMapper(float Value)
{
	switch (axisName) {
	case 1: { AddMovementInput(GetActorForwardVector(), Value); break; }
	case 2: { AddMovementInput(GetActorRightVector(), Value); break; }
	case 3: { APawn::AddControllerYawInput(Value); break; }
	case 4: { APawn::AddControllerPitchInput(Value); break; }
	// case 5: {
	// 	FQuat res = FQuat(FVector::UpVector, Value/10);
	// 	UE_LOG(LogTemp, Warning, TEXT("Yeet: Rotation matrix was %d, %d, %d, %d"), res.X, res.Y, res.Z, res.W);
	// 	_camera->AddLocalRotation(res);
	// 	break;
	// }
	// case 6: {
	// 	FQuat res = FQuat(FVector::UpVector, Value/10);
	// 	UE_LOG(LogTemp, Warning, TEXT("YeetNot : Rotation matrix was %d, %d, %d, %d"), res.X, res.Y, res.Z, res.W);
	// 	_camera->AddLocalRotation(res);
	// 	break;
	// }
	// default: {break; }
	}
}

void AWW_Player::ReactionMapper(EResponses in) {
	switch(in) {
	case EResponses::DoNothing : {return; }
	case EResponses::StartSprint : {
		FString str = "action sprinting mapping";
		GetCharacterMovement()->MaxWalkSpeed = jogSpeed;
		CameraShakeTimerMap(1, true);
		CameraShakeMap(1, true);
		//GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Red, FString::Printf(TEXT("%s"), *(str)));
		break;
	}
	case EResponses::StopSprint: {
		GetCharacterMovement()->MaxWalkSpeed = walkSpeed;
		CameraShakeTimerMap(1, false);
		break;
	}
	case EResponses::StartDashing: {
		
	}
	case EResponses::DashNotAvailable: {
		
	}
	default: {}
	}
}

//Sets up and clear timers
void AWW_Player::CameraShakeTimerMap(int Index, bool Timerkey)
{
	switch (Index) {
	case(1): {
		if (Timerkey) {
			CameraShake_TDelegate.BindUFunction(this, FName("CameraShakeMap"), 1, true);
			GetWorldTimerManager().SetTimer(CameraShake_THandle, CameraShake_TDelegate, 1.5f, true);
			return;
		}
		else {
			GetWorld()->GetTimerManager().ClearTimer(CameraShake_THandle);
		}
		break;
	}
	default: {break; }
	}
}

void AWW_Player::CameraShakeMap(int Index, bool key)
{
	switch (Index) {
	case 1: {
		if (key) {
			//ControllerRef->ClientPlayCameraShake(UWW_PlayerCameraShake::StaticClass(), 1.0f, ECameraAnimPlaySpace::CameraLocal);
		}
		else {
			//ControllerRef->ClientStopCameraShake(UWW_PlayerCameraShake::StaticClass(), true);
			GetWorld()->GetTimerManager().ClearTimer(CameraShake_THandle);
		}
		break;
	}
	}
}

void AWW_Player::CameraZoomManager(float value)
{
	FQuat res = FQuat(FVector::UpVector, value / 10);
	// UE_LOG(LogTemp, Warning, TEXT("YeetNot : Rotation matrix was %d, %d, %d, %d"), res.X, res.Y, res.Z, res.W);
	_camera->AddLocalRotation(res);
}

bool AWW_Player::GiveEffect(TSubclassOf<UGameplayEffect> In) {
	FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
	EffectContext.AddSourceObject(this);

	FGameplayEffectSpecHandle NewHandle = AbilitySystemComponent->MakeOutgoingSpec(In, 1, EffectContext);
	if (NewHandle.IsValid()) {
		FActiveGameplayEffectHandle ActiveGEHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToTarget(*NewHandle.Data.Get(), AbilitySystemComponent);
	}
	return NewHandle.IsValid();
}

bool AWW_Player::GiveAbility(TSubclassOf<UGameplayAbility> In, int KeyIndex) {
	AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(In, 1, KeyIndex, this));
	return true;
}

void AWW_Player::HandleHealthChanged(float DeltaValue, const FGameplayTagContainer& EventTags)
{
	if (bAbilitiesInitialized) {
		UE_LOG(LogTemp, Warning, TEXT("Yes. The abilities were initialized"));
	}
}

// Called to bind functionality to input
void AWW_Player::SetupPlayerInputComponent(UInputComponent* InputComp)
{
	Super::SetupPlayerInputComponent(InputComp);

	// set up gameplay key bindings
	// check(InputComp);

	InputComp->BindAction("Sprint", IE_Pressed, this, &AWW_Player::ActionMapper<EPlayerActions::Sprint,1>);
	InputComp->BindAction("Sprint", IE_Released, this, &AWW_Player::ActionMapper<EPlayerActions::Sprint,0>);
	InputComp->BindAction("Jump", IE_Pressed, this, &AWW_Player::ActionMapper<EPlayerActions::Jump, 1>);
	InputComp->BindAction("Jump", IE_Released, this, &AWW_Player::ActionMapper<EPlayerActions::Jump, 0>);
	
	InputComp->BindAxis("MoveForward", this, &AWW_Player::AxisMapper<1>);
	InputComp->BindAxis("MoveRight", this, &AWW_Player::AxisMapper<2>);
	InputComp->BindAxis("Turn", this, &AWW_Player::AxisMapper<3>);
	InputComp->BindAxis("LookUp", this, &AWW_Player::AxisMapper<4>);

	// InputComp->BindAxis("CameraZoomIn", this, &AWW_Player::AxisMapper<5>);
	// InputComp->BindAxis("CameraZoomOut", this, &AWW_Player::AxisMapper<6>);
	InputComp->BindAxis("CameraZoomIn", this, &AWW_Player::CameraZoomManager);
	InputComp->BindAxis("CameraZoomOut", this, &AWW_Player::CameraZoomManager);

	// This can be found at...
	AbilitySystemComponent->BindAbilityActivationToInputComponent(InputComponent, FGameplayAbilityInputBinds("ConfirmInput", "CancelInput", "AbilityInput"));
}