// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "DashAbility.generated.h"

/**
 * 
 */
UCLASS()
class UDashAbility : public UGameplayAbility
{
	GENERATED_BODY()

public:
	UDashAbility(const FObjectInitializer& ObjectInitializer);

	virtual bool CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags = nullptr, const FGameplayTagContainer* TargetTags = nullptr, OUT FGameplayTagContainer* OptionalRelevantTags = nullptr) const override;

	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* OwnerInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

	// Key handling
	//virtual void InputPressed(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo) override;
	virtual void InputReleased(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo) override;

	virtual void CancelAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateCancelAbility) override;

	/* Useful overrides

	// Used to get SkeletalMesh for animations...
	virtual USkeletalMeshComponent* GetOwningComponentFromActorInfo() const override;

	// Used for cooldown based abilities
	virtual void GetCooldownTimeRemainingAndDuration(FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, float& TimeRemaining, float& CooldownDuration) const;
	
	// Sets rather ability block flags are enabled or disabled. Only valid on instanced abilities (has a getter)
	virtual void SetShouldBlockOtherAbilities(bool bShouldBlockAbilities);
	
	// Can ability be cancelled. Applies for instanced abilities.Has getter, setter
	virtual void SetCanBeCanceled(bool bCanBeCanceled);

	// Commitability : Was hard to understand... Onto the next section.

	// Checks the ability's cooldown, but does not apply it.
	UFUNCTION(BlueprintCallable, Category = Ability, DisplayName = "CheckAbilityCooldown", meta = (ScriptName = "CheckAbilityCooldown"))
		virtual bool K2_CheckAbilityCooldown();

	// Checks the ability's cost, but does not apply it.
	UFUNCTION(BlueprintCallable, Category = Ability, DisplayName = "CheckAbilityCost", meta = (ScriptName = "CheckAbilityCost"))
		virtual bool K2_CheckAbilityCost();

	virtual bool CommitAbilityCooldown(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const bool ForceCooldown);
	virtual bool CommitAbilityCost(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo);

	// Does the commit atomically (consume resources, do cooldowns, etc)
	virtual void CommitExecute(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo);
	
	// Commitability : Was not useful for now... Onto the next section.

	// Called when the ability is given to an AbilitySystemComponent
	virtual void OnGiveAbility(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec);

	// Removes the GameplayEffect that granted this ability. Can only be called on instanced abilities. Might be interesting
	virtual void RemoveGrantedByEffect();

	// Adds a debug message to display to the user. This will be needed.
	void AddAbilityTaskDebugMessage(UGameplayTask* AbilityTask, FString DebugMessage);

	// IGameplayTaskOwnerInterface : Was not useful for now... Onto the next section.
	/**/
};
