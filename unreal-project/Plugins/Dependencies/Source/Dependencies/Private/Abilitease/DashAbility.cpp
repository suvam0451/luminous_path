// Fill out your copyright notice in the Description page of Project Settings.


#include "DashAbility.h"
#include "GameFramework/Character.h"
#include "GameFramework/PawnMovementComponent.h"
#include "GameFramework/CharacterMovementComponent.h"


UDashAbility::UDashAbility(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Want jump to be visible to all players on network
	NetExecutionPolicy = EGameplayAbilityNetExecutionPolicy::LocalPredicted;
	// Want jumping to be instanced...
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;
}

bool UDashAbility::CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags, const FGameplayTagContainer* TargetTags, OUT FGameplayTagContainer* OptionalRelevantTags) const
{
	if (!Super::CanActivateAbility(Handle, ActorInfo, SourceTags, TargetTags, OptionalRelevantTags))
	{
		return false;
	}

	const ACharacter* Character = CastChecked<ACharacter>(ActorInfo->AvatarActor.Get(), ECastCheckedType::NullAllowed);
	return (Character && Character->CanJump());
}

void UDashAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	if (HasAuthorityOrPredictionKey(ActorInfo, &ActivationInfo))
	{
		if (!CommitAbility(Handle, ActorInfo, ActivationInfo))
		{
			return;
		}

		// This ability makes the player dash in the given direction...
		// We are assuming the actor is a character (Not a pawn)
		UE_LOG(LogTemp, Warning, TEXT("Lrelease the krtaken"));
		ACharacter* Character = CastChecked<ACharacter>(ActorInfo->AvatarActor.Get());
		//Character->Jump();
		UCharacterMovementComponent* movecomp = Cast<UCharacterMovementComponent>(Character->GetMovementComponent());
		movecomp->MaxWalkSpeed = 1000;
	}
}

//void UDashAbility::InputPressed(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo)
//{
//	UE_LOG(LogTemp, Warning, TEXT("It calls InputPressed here"));
//	if (ActorInfo != NULL && ActorInfo->AvatarActor != NULL) {
//		UE_LOG(LogTemp, Warning, TEXT("Okay then. Start ability"));
//	}
//}

void UDashAbility::InputReleased(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo)
{
	if (ActorInfo != NULL && ActorInfo->AvatarActor != NULL)
	{
		CancelAbility(Handle, ActorInfo, ActivationInfo, true);
	}
}

void UDashAbility::CancelAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateCancelAbility)
{
	if (ScopeLockCount > 0)
	{
		WaitingToExecute.Add(FPostLockDelegate::CreateUObject(this, &UDashAbility::CancelAbility, Handle, ActorInfo, ActivationInfo, bReplicateCancelAbility));
		return;
	}

	Super::CancelAbility(Handle, ActorInfo, ActivationInfo, bReplicateCancelAbility);

	UE_LOG(LogTemp, Warning, TEXT("Retract the krtaken"));
	ACharacter* Character = CastChecked<ACharacter>(ActorInfo->AvatarActor.Get());
	//Character->StopJumping();
	UCharacterMovementComponent* movecomp = Cast<UCharacterMovementComponent>(Character->GetMovementComponent());
	movecomp->MaxWalkSpeed = 400;
}
