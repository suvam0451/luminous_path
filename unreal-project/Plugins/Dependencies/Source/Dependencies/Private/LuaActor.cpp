// Fill out your copyright notice in the Description page of Project Settings.
#include "LuaActor.h"
#include <string>

// Sets default values
ALuaActor::ALuaActor()
{
	PrimaryActorTick.bCanEverTick = false;
	SceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRoot"));
	RootComponent = SceneRoot;
	SceneRoot->SetMobility(EComponentMobility::Type::Static);
}

void ALuaActor::OnConstruction(const FTransform& Transform) {
	// lua_State* L = luaL_newstate();
	// luaL_openlibs(L);
// 
	// int x;
	// if(CheckLua(L, luaL_dofile(L, "lewdheh.lua"))) {
	// 	lua_getglobal(L, "x");
	// 	if (lua_isnumber(L, -1)){
	// 		x = lua_tonumber(L, -1);	
	// 		UE_LOG(LogTemp, Warning, TEXT("%d"), x);
	// 	}	
	// }
}

// Called when the game starts or when spawned
void ALuaActor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ALuaActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Helper function
bool ALuaActor::CheckLua(lua_State* L, int r) {
	if (r != LUA_OK) {
		std::string errormsg = lua_tostring(L, -1);
		// std::cout << errormsg << std::endl;
		// FString err(errormsg.c_str());
		FString err = UTF8_TO_TCHAR(errormsg.c_str());
		// UE_LOG(LogTemp, Warning, TEXT("Lua error: %s"), err);
		UE_LOG(LogTemp, Warning, TEXT("oh man, not again"));
		return false;
	}
	return true;
}