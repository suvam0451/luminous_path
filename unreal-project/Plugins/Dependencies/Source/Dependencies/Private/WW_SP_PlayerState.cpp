// Fill out your copyright notice in the Description page of Project Settings.


#include "WW_SP_PlayerState.h"

AWW_SP_PlayerState::AWW_SP_PlayerState() {
	AbilitySystemComponent = CreateDefaultSubobject<UAbilitySystemComponent>(TEXT("Ability System Component"));
	AttributeSet = CreateDefaultSubobject<UWW_Attributes>(TEXT("Attribute Set"));
}

void AWW_SP_PlayerState::BeginPlay() {
	Super::BeginPlay();
	// if (HasAuthority()) {
	// 	GiveAbilities();
	// }
}

// Returns our ability system component.
UAbilitySystemComponent* AWW_SP_PlayerState::GetAbilitySystemComponent() const {
	return AbilitySystemComponent;
}

// Returns our attribute set.
UWW_Attributes* AWW_SP_PlayerState::GetAttributeSet() const {
	return AttributeSet;
}


// void AWW_SP_PlayerState::GiveAbilities_Implementation() (Why was Implementation appended ?)
void AWW_SP_PlayerState::GiveAbilities()
{	
	if (!HasAuthority()) { return; }
	int index = 0;
	for (auto it : AbilityStack) {
		if (IsValid(it)) {
			this->AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(it.GetDefaultObject(), 1, index));
		}
		index++;
	}
}

// Checks to make sure we have authority and an ability then gives the specified ability.
//void AWW_SP_PlayerState::GiveGameplayAbility_Implementation(TSubclassOf<UGameplayAbility> AbilityToGive)
void AWW_SP_PlayerState::GiveGameplayAbility(TSubclassOf<UGameplayAbility> AbilityToGive)
{
	if (HasAuthority() && AbilityToGive)
	{
		this->AbilitySystemComponent->GiveAbility(AbilityToGive);
	}
}
