// Fill out your copyright notice in the Description page of Project Settings.


#include "Prototypes/WW_AsyncActor.h"

// Sets default values
AWW_AsyncActor::AWW_AsyncActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AWW_AsyncActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWW_AsyncActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

