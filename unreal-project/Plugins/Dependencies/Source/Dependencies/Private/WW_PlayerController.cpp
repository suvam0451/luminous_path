// Fill out your copyright notice in the Description page of Project Settings.


#include "WW_PlayerController.h"
#include <algorithm>

bool AWW_PlayerController::AddInventoryItem(URPGItem* NewItem, int32 ItemCount, int32 ItemLevel, bool bAutoSlot)
{
	bool bChanged = false;
	if (!NewItem) {
		UE_LOG(LogTemp, Warning, TEXT("AddInventoryItem: Failed trying to add null item!"));
		return false;
	}

	if (ItemCount <= 0 || ItemLevel <= 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("AddInventoryItem: Failed trying to add item %s with negative count or level!"), *NewItem->GetName());
		return false;
	}

	// Find current item data, which may be empty
	FWW_ItemData OldData;
	GetInventoryItemData(NewItem, OldData);

	// Find modified data
	FWW_ItemData NewData = OldData;
	NewData.UpdateItemData(FWW_ItemData(ItemCount, ItemLevel), NewItem->MaxCount, NewItem->MaxLevel);
	
	if (OldData != NewData)
	{
		// If data changed, need to update storage and call callback
		InventoryData.Add(NewItem, NewData);
		NotifyInventoryItemChanged(true, NewItem);
		bChanged = true;
	}

	if (bAutoSlot)
	{
		// Slot item if required
		bChanged |= FillEmptySlotWithItem(NewItem);
	}

	if (bChanged)
	{
		// If anything changed, write to save game
		SaveInventory();
		return true;
	}
	return false;
}

bool AWW_PlayerController::RemoveInventoryItem(URPGItem* RemovedItem, int32 RemoveCount)
{
	if (!RemovedItem) {
		UE_LOG(LogTemp, Warning, TEXT("RemoveInventoryItem: Failed trying to remove null item!"));
		return false;
	}	
	
	// Find current item data, which may be empty
	FWW_ItemData NewData;
	GetInventoryItemData(RemovedItem, NewData);

	if (!NewData.IsValid())
	{
		// Wasn't found
		return false;
	}

	// If RemoveCount <= 0, delete all
	(RemoveCount <= 0) ? (NewData.ItemCount = 0) : (NewData.ItemCount -= RemoveCount);

	if (NewData.ItemCount > 0)
	{
		// Update data with new count
		InventoryData.Add(RemovedItem, NewData);
	}
	else
	{
		// Remove item entirely, make sure it is unslotted
		InventoryData.Remove(RemovedItem);

		for (TPair<FWW_ItemSlot, URPGItem*>& Pair : SlottedItems)
		{
			if (Pair.Value == RemovedItem)
			{
				Pair.Value = nullptr;
				NotifySlottedItemChanged(Pair.Key, Pair.Value);
			}
		}
	}

	// If we got this far, there is a change so notify and save
	NotifyInventoryItemChanged(false, RemovedItem);

	SaveInventory();
	return true;
}

void AWW_PlayerController::GetInventoryItems(TArray<URPGItem*>& Items, FPrimaryAssetType ItemType)
{
	for (const TPair<URPGItem*, FWW_ItemData>& Pair : InventoryData)
	{
		if (Pair.Key)
		{
			FPrimaryAssetId AssetId = Pair.Key->GetPrimaryAssetId();

			// Filters based on item type
			if (AssetId.PrimaryAssetType == ItemType || !ItemType.IsValid())
			{
				Items.Add(Pair.Key);
			}
		}
	}
}

int32 AWW_PlayerController::GetInventoryItemCount(URPGItem* Item) const
{
	const FWW_ItemData* FoundItem = InventoryData.Find(Item);
	return FoundItem ? (FoundItem->ItemCount) : 0;
	// return int32();
}

bool AWW_PlayerController::GetInventoryItemData(URPGItem* Item, FWW_ItemData& ItemData) const
{
	const FWW_ItemData* FoundItem = InventoryData.Find(Item);
	ItemData = FoundItem ? *FoundItem : FWW_ItemData(0, 0);
	return FoundItem ? true : false;
}

bool AWW_PlayerController::SetSlottedItem(FWW_ItemSlot ItemSlot, URPGItem* Item)
{
	bool bFound = false;
	for (auto Pair : SlottedItems) {
		if (Pair.Key == ItemSlot) {
			// Add to new slot
			bFound = true;
			Pair.Value = Item;
			NotifySlottedItemChanged(Pair.Key, Pair.Value);
		}
		else if (Item != nullptr && Pair.Value == Item) {
			// If this item was found in another slot, remove it
			Pair.Value = nullptr;
			NotifySlottedItemChanged(Pair.Key, Pair.Value);
		}
	}
	bFound ? SaveInventory() : true;
	return bFound ? true : false;
}

URPGItem* AWW_PlayerController::GetSlottedItem(FWW_ItemSlot ItemSlot) const
{
	URPGItem* const* FoundItem = SlottedItems.Find(ItemSlot);
	return FoundItem ? *FoundItem : nullptr;
}

void AWW_PlayerController::GetSlottedItems(TArray<URPGItem*>& Items, FPrimaryAssetType ItemType, bool bOutputEmptyIndexes)
{
	for (auto& Pair : SlottedItems) {
		// Add if does not already exits/not valid
		(Pair.Key.ItemType == ItemType || !ItemType.IsValid()) ? Items.Add(Pair.Value) : true;
	}
}

void AWW_PlayerController::FillEmptySlots()
{
	bool bShouldSave = false;
	for (auto& Pair : InventoryData) {
		bShouldSave |= FillEmptySlotWithItem(Pair.Key);
	}
	bShouldSave ? SaveInventory() : true;
}

bool AWW_PlayerController::SaveInventory()
{
	UWorld* World = GetWorld();
	return false;
}

bool AWW_PlayerController::LoadInventory()
{
	return false;
}

bool AWW_PlayerController::FillEmptySlotWithItem(URPGItem* NewItem)
{
	// Look for an empty item slot to fill with this item
	FPrimaryAssetType NewItemType = NewItem->GetPrimaryAssetId().PrimaryAssetType;
	FWW_ItemSlot EmptySlot;
	for (auto& Pair : SlottedItems)
	{
		if (Pair.Key.ItemType == NewItemType)
		{
			if (Pair.Value == NewItem)
			{
				// Item is already slotted
				return false;
			}
			else if (Pair.Value == nullptr && (!EmptySlot.IsValid() || EmptySlot.SlotNumber > Pair.Key.SlotNumber))
			{
				// We found an empty slot worth filling
				EmptySlot = Pair.Key;
			}
		}
	}

	if (EmptySlot.IsValid())
	{
		SlottedItems[EmptySlot] = NewItem;
		NotifySlottedItemChanged(EmptySlot, NewItem);
		return true;
	}

	return false;
}

void AWW_PlayerController::NotifyInventoryItemChanged(bool bAdded, URPGItem* Item)
{	
	// Notify native before blueprint
	OnInventoryItemChangedNative.Broadcast(bAdded, Item);
	OnInventoryItemChanged.Broadcast(bAdded, Item);

	// Call BP update event
	InventoryItemChanged(bAdded, Item);
}

void AWW_PlayerController::NotifySlottedItemChanged(FWW_ItemSlot ItemSlot, URPGItem* Item)
{
	// Notify native before blueprint
	OnSlottedItemChangedNative.Broadcast(ItemSlot, Item);
	OnSlottedItemChanged.Broadcast(ItemSlot, Item);

	// Call BP update event
	SlottedItemChanged(ItemSlot, Item);
}

void AWW_PlayerController::NotifyInventoryLoaded()
{
	// Notify native before blueprint
	OnInventoryLoadedNative.Broadcast();
	OnInventoryLoaded.Broadcast();
}

void AWW_PlayerController::BeginPlay() {
	// Load inventory off save game before starting play
	LoadInventory();
	Super::BeginPlay();
}