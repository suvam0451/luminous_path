// Fill out your copyright notice in the Description page of Project Settings.


#include "WW_EngineOverrides.h"
#include "AbilitySystemGlobals.h"

#pragma region AssetRegistry Override

const FPrimaryAssetType	UWW_AssetManager::PotionItemType = TEXT("Potion");
const FPrimaryAssetType	UWW_AssetManager::SkillItemType = TEXT("Skill");
//const FPrimaryAssetType	UWW_AssetManager::TokenItemType = TEXT("Token");
const FPrimaryAssetType	UWW_AssetManager::WeaponItemType = TEXT("Weapon");

UWW_AssetManager& UWW_AssetManager::Get()
{
	UWW_AssetManager* This = Cast<UWW_AssetManager>(GEngine->AssetManager);

	if (This)
	{
		return *This;
	}
	else
	{
		// This segment will get called, if our config file does not list the Custom AssetManager...
		// UE_LOG(LogActionRPG, Fatal, TEXT("Invalid AssetManager in DefaultEngine.ini, must be RPGAssetManager!"));
		UE_LOG(LogTemp, Warning, TEXT("It seems you did not set the AssetRegistry override. Please follow docs for error."));
		return *NewObject<UWW_AssetManager>(); // never calls this
	}
}

void UWW_AssetManager::StartInitialLoading()
{
	Super::StartInitialLoading();
	UAbilitySystemGlobals::Get().InitGlobalData();
}

URPGItem* UWW_AssetManager::ForceLoadItem(const FPrimaryAssetId& PrimaryAssetId, bool bLogWarning)
{
	FSoftObjectPath ItemPath = GetPrimaryAssetPath(PrimaryAssetId);

	// This does a synchronous load and may hitch
	URPGItem* LoadedItem = Cast<URPGItem>(ItemPath.TryLoad());

	if (bLogWarning && LoadedItem == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to load item for identifier %s. It seems you did not set the AssetRegistry override. Please follow docs for error!"), *PrimaryAssetId.ToString());
	}

	return LoadedItem;
}

#pragma endregion

