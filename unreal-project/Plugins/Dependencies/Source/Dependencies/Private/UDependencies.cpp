// Copyright ‎© 2019 Debashish Patra, released under Mozilla Public License, version 2.0.
// See file LICENSE.md for full license details.

#include "UDependencies.h"
#include "Paths.h"
#include "PlatformProcess.h"
#include "Runtime/Launch/Resources/Version.h"
#include "Runtime/RenderCore/Public/ShaderCore.h"

#define LOCTEXT_NAMESPACE "FDependencies"

void FDependencies::StartupModule(){

    // This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	#if (ENGINE_MINOR_VERSION > 21)    
		UE_LOG(LogTemp, Warning, TEXT("Path is %s"), *FPaths::Combine(*(FPaths::ProjectPluginsDir())));
    	AddShaderSourceDirectoryMapping(TEXT("/WW"), FPaths::Combine(*(FPaths::ProjectPluginsDir()), TEXT("Dependencies/Shaders")));
		//FPaths::Plugin
	#endif


    FPaths path;
    FString DirName = path.ProjectPluginsDir();
    FPaths::Combine(DirName, "Dependencies", "ThirdParty", "lib", "downtown_dll.dll");
    DLLHandle = FPlatformProcess::GetDllHandle(*DirName);

    if(DLLHandle == nullptr){
        UE_LOG(LogTemp, Warning, TEXT("WinterWildfile module failed to load DLL. Please check your paths"));
    } 
}   
void FDependencies::ShutdownModule(){
    FPlatformProcess::FreeDllHandle(DLLHandle);
}
IMPLEMENT_MODULE(FDependencies, Dependencies)
#undef LOCTEXT_NAMESPACE