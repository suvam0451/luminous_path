// Fill out your copyright notice in the Description page of Project Settings.


#include "GAS/WW_Attributes.h"
#include "GameplayEffect.h"
#include "GameplayEffectExtension.h"
#include "UnrealNetwork.h"
#include "WW_Player.h"
//#include <AttributeSetBase.cpp>

UWW_Attributes::UWW_Attributes() {
	Health = 1.0f;
	MaxHealth = 1.0f;
}

void UWW_Attributes::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{
	// This is called whenever attributes change, so for max health/mana we want to scale the current totals to match
	Super::PreAttributeChange(Attribute, NewValue);
	// Enumerate possibilities and assign handler classes
	(Attribute == GetMaxHealthAttribute()) ? AdjustAttributeForMaxChange(Health, MaxHealth, NewValue, GetHealthAttribute()) : true;
}

void UWW_Attributes::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);

	FGameplayEffectContextHandle Context = Data.EffectSpec.GetContext();
	UAbilitySystemComponent* Source = Context.GetOriginalInstigatorAbilitySystemComponent();
	const FGameplayTagContainer& SourceTags = *Data.EffectSpec.CapturedSourceTags.GetAggregatedTags();

	// "DeltaValue" useful for only additive case.
	float DeltaValue = (Data.EvaluatedData.ModifierOp == EGameplayModOp::Type::Additive) ? DeltaValue = Data.EvaluatedData.Magnitude : 0;
	
	
	// Get the Target actor, which should be our owner
	AActor* TargetActor = nullptr;
	//AController* TargetController = nullptr;
	APlayerController* TargetController = nullptr;
	//ARPGCharacterBase* TargetCharacter = nullptr;
	AWW_Player* TargetCharacter = nullptr;
	if (Data.Target.AbilityActorInfo.IsValid() && Data.Target.AbilityActorInfo->AvatarActor.IsValid())
	{
		TargetActor = Data.Target.AbilityActorInfo->AvatarActor.Get();
		TargetController = Data.Target.AbilityActorInfo->PlayerController.Get();
		//TargetCharacter = Cast<ARPGCharacterBase>(TargetActor);
		TargetCharacter = Cast<AWW_Player>(TargetActor);
	}

	if (Data.EvaluatedData.Attribute == GetHealthAttribute())
	{
		// Handle other health changes such as from healing or direct modifiers. First clamp it
		SetHealth(FMath::Clamp(GetHealth(), 0.0f, GetMaxHealth()));

		// Character class handles attribute change
		TargetCharacter ? TargetCharacter->HandleHealthChanged(DeltaValue, SourceTags) : true;
	}
}

void UWW_Attributes::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UWW_Attributes, Health);
	DOREPLIFETIME(UWW_Attributes, MaxHealth);
}

void UWW_Attributes::OnRep_Health()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UWW_Attributes, Health);
}

void UWW_Attributes::OnRep_MaxHealth()
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UWW_Attributes, MaxHealth);
}

void UWW_Attributes::AdjustAttributeForMaxChange(FGameplayAttributeData& AffectedAttribute, const FGameplayAttributeData& MaxAttribute, float NewMaxValue, const FGameplayAttribute& AffectedAttributeProperty)
{
	UAbilitySystemComponent* AbilityComp = GetOwningAbilitySystemComponent();
	const float CurrentMaxValue = MaxAttribute.GetCurrentValue();
	if (!FMath::IsNearlyEqual(CurrentMaxValue, NewMaxValue) && AbilityComp)
	{
		// Change current value to maintain the current Val / Max percent
		const float CurrentValue = AffectedAttribute.GetCurrentValue();
		float NewDelta = (CurrentMaxValue > 0.f) ? (CurrentValue * NewMaxValue / CurrentMaxValue) - CurrentValue : NewMaxValue;

		AbilityComp->ApplyModToAttributeUnsafe(AffectedAttributeProperty, EGameplayModOp::Additive, NewDelta);
	}
}
