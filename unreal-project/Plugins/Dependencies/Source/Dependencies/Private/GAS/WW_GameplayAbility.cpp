// Fill out your copyright notice in the Description page of Project Settings.


#include "WW_GameplayAbility.h"
#include "Dependencies/Public/GAS/WW_AbilitySysComp.h"

UWW_GameplayAbility::UWW_GameplayAbility() {}

FWW_GameplayEffectContainerSpec UWW_GameplayAbility::MakeEffectContainerSpecFromContainer(const FWW_GameplayEffectContainer& Container, const FGameplayEventData& EventData, int32 OverrideGameplayLevel) {

	// First figure out the actor info
	FWW_GameplayEffectContainerSpec ReturnSpec;
	AActor* OwningActor = GetOwningActorFromActorInfo();
	AWW_Player* OwningCharacter = Cast<AWW_Player>(OwningActor);
	UWW_AbilitySysComp* OwningASC = UWW_AbilitySysComp::GetAbilitySystemComponentFromActor(OwningActor);

	if (OwningASC) {
		
		// If we have a target type, run the targeting logic. This is optional, targets can be added later
		if (Container.TargetType.Get()) {
			TArray<FHitResult> HitResults;
			TArray<AActor*> TargetActors;
			const UWW_TargetType* TargetTypeCDO = Container.TargetType.GetDefaultObject();
			AActor* AvatarActor = GetAvatarActorFromActorInfo();
			TargetTypeCDO->GetTargets(OwningCharacter, AvatarActor, EventData, HitResults, TargetActors);
			ReturnSpec.AddTargets(HitResults, TargetActors);
		}

		// If we don't have an override level, use the default on the ability itself
		if (OverrideGameplayLevel == INDEX_NONE)
		{
			OverrideGameplayLevel = OverrideGameplayLevel = this->GetAbilityLevel(); //OwningASC->GetDefaultAbilityLevel();
		}

		// Build GameplayEffectSpecs for each applied effect
		for (auto& EffectClass : Container.TargetGameplayEffectClasses) {
			ReturnSpec.TargetGameplayEffectSpecs.Add(MakeOutgoingGameplayEffectSpec(EffectClass, OverrideGameplayLevel));
		}
	}
	return ReturnSpec;
}

FWW_GameplayEffectContainerSpec UWW_GameplayAbility::MakeEffectContainerSpec(FGameplayTag ContainerTag, const FGameplayEventData& EventData, int32 OverrideGameplayLevel) {
	FWW_GameplayEffectContainer* FoundContainer = EffectContainerMap.Find(ContainerTag);

	if (FoundContainer) {
		return MakeEffectContainerSpecFromContainer(*FoundContainer, EventData, OverrideGameplayLevel);
	}
	return FWW_GameplayEffectContainerSpec();
}

TArray<FActiveGameplayEffectHandle> UWW_GameplayAbility::ApplyEffectContainerSpec(const FWW_GameplayEffectContainerSpec& ContainerSpec)
{
	TArray<FActiveGameplayEffectHandle> AllEffects;

	// Iterate list of effect specs and apply them to their target data
	for (const FGameplayEffectSpecHandle& SpecHandle : ContainerSpec.TargetGameplayEffectSpecs)
	{
		AllEffects.Append(K2_ApplyGameplayEffectSpecToTarget(SpecHandle, ContainerSpec.TargetData));
	}
	return AllEffects;
}

TArray<FActiveGameplayEffectHandle> UWW_GameplayAbility::ApplyEffectContainer(FGameplayTag ContainerTag, const FGameplayEventData& EventData, int32 OverrideGameplayLevel) {
	FWW_GameplayEffectContainerSpec Spec = MakeEffectContainerSpec(ContainerTag, EventData, OverrideGameplayLevel);
	return ApplyEffectContainerSpec(Spec);
}