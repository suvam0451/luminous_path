// Copyright Debashish Patra

#include "Dependencies/Public/GAS/WW_TargetTypes.h"

void UWW_TargetType::GetTargets_Implementation(AWW_Player* TargetingCharacter, AActor* TargetingActor, FGameplayEventData EventData, TArray<FHitResult>& OutHitResults, TArray<AActor*>& OutActors) const
{
	UE_LOG(LogTemp, Warning, TEXT("Blueprint implementation of base TargetType class is called"));
}

void UWW_TargetType_UseOwner::GetTargets_Implementation(AWW_Player* TargetingCharacter, AActor* TargetingActor, FGameplayEventData EventData, TArray<FHitResult>& OutHitResults, TArray<AActor*>& OutActors) const
{
	UE_LOG(LogTemp, Warning, TEXT("Blueprint implementation of UseOwner TargetType class is called"));
	OutActors.Add(TargetingCharacter);
}

void UWW_TargetType_UseEventData::GetTargets_Implementation(AWW_Player* TargetingCharacter, AActor* TargetingActor, FGameplayEventData EventData, TArray<FHitResult>& OutHitResults, TArray<AActor*>& OutActors) const
{
	UE_LOG(LogTemp, Warning, TEXT("Blueprint implementation of UseEventData TargetType class is called"));
	const FHitResult* FoundHitResult = EventData.ContextHandle.GetHitResult();
	if (FoundHitResult)
	{
		OutHitResults.Add(*FoundHitResult);
	}
	else if (EventData.Target)
	{
		OutActors.Add(const_cast<AActor*>(EventData.Target));
	}
}