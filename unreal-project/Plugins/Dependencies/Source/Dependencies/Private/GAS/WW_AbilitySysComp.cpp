// Fill out your copyright notice in the Description page of Project Settings.


#include "GAS/WW_AbilitySysComp.h"
#include "WW_Player.h"

//#include "Abilities/RPGAbilitySystemComponent.h"
//#include "RPGCharacterBase.h"
//#include "Abilities/RPGGameplayAbility.h"
#include "AbilitySystemGlobals.h"

UWW_AbilitySysComp::UWW_AbilitySysComp() {}

//void UWW_AbilitySysComp::GetActiveAbilitiesWithTags(const FGameplayTagContainer& GameplayTagContainer, TArray<URPGGameplayAbility*>& ActiveAbilities)
//{
//	TArray<FGameplayAbilitySpec*> AbilitiesToActivate;
//	GetActivatableGameplayAbilitySpecsByAllMatchingTags(GameplayTagContainer, AbilitiesToActivate, false);
//
//	// Iterate the list of all ability specs
//	for (FGameplayAbilitySpec* Spec : AbilitiesToActivate)
//	{
//		// Iterate all instances on this ability spec
//		TArray<UGameplayAbility*> AbilityInstances = Spec->GetAbilityInstances();
//
//		for (UGameplayAbility* ActiveAbility : AbilityInstances)
//		{
//			ActiveAbilities.Add(Cast<URPGGameplayAbility>(ActiveAbility));
//		}
//	}
//}

int32 UWW_AbilitySysComp::GetDefaultAbilityLevel() const
{
	AWW_Player* OwningCharacter = Cast<AWW_Player>(OwnerActor);

	if (OwningCharacter)
	{
		//return OwningCharacter->GetCharacterLevel();
	}
	return 1;
}

UWW_AbilitySysComp* UWW_AbilitySysComp::GetAbilitySystemComponentFromActor(const AActor* Actor, bool LookForComponent)
{
	return Cast<UWW_AbilitySysComp>(UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(Actor, LookForComponent));
}
