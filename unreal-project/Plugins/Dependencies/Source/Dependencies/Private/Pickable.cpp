// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickable.h"

// Sets default values
APickable::APickable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void APickable::BeginPlay()
{
	Super::BeginPlay();
}


void APickable::OnInteract(AActor* Caller)
{
	UE_LOG(LogTemp, Warning, TEXT("On interact is called"));
	this->Destroy();
}

void APickable::StartFocus()
{
	UE_LOG(LogTemp, Warning, TEXT("Start Focus is called"));
}


void APickable::EndFocus()
{
	UE_LOG(LogTemp, Warning, TEXT("End Focus is called"));
}


