// Copyright ‎© 2019 Debashish Patra, released under Mozilla Public License, version 2.0.
// See file LICENSE.md for full license details.

using UnrealBuildTool;
using System.IO;

public class Dependencies : ModuleRules
{
	// Get ThirdParty lib path
	private string ThirdPartyPath() {
		return Path.GetFullPath(Path.Combine(PluginDirectory, "ThirdParty/"));
	} 


    public Dependencies(ReadOnlyTargetRules Target) : base(Target)
	{
        // Get project location...
        var basePath = Path.GetDirectoryName(RulesCompiler.GetFileNameFromType(GetType()));
        // check if debug mode...
        // bool bDebug = (Target.Configuration == UnrealTargetConfiguration.Debug && BuildConfiguration.bDebugBuildsActuallyUseDebugCRT);

        bFasterWithoutUnity = true;
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "Public"));
        PrivateIncludePaths.Add(Path.Combine(ModuleDirectory, "Private"));			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"UMG"
				// ... add other public dependencies that you statically link with here ...
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
				"RenderCore",
				"InputCore",
				"GameplayAbilities",
                "GameplayTags",
				"GameplayTasks"
				// ... add private dependencies that you statically link with here ...	
			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
			);

        // if (Target.Platform == UnrealTargetPlatform.Win64) {
        // Loads required DLL...
        /* tell build system that we want to manually load our DLL...
            @ But affirm that the DLL must exist
        */
        PublicDelayLoadDLLs.Add("downtown_dll.dll");

			string DLLpath = Path.Combine(PluginDirectory, "ThirdParty/lib", "downtown_dll.dll");
			// Loads required LIB...
			PublicAdditionalLibraries.Add(Path.Combine(PluginDirectory, "ThirdParty/lib", "downtown_dll.lib"));
			PublicIncludePaths.Add(Path.Combine(PluginDirectory, "ThirdParty/include/FunctionLibrary.h"));

			// Adding our DLL as a runtime dependency...
    		RuntimeDependencies.Add(new RuntimeDependency(DLLpath));
			
			// Add dependency to shipping...
			string binariesDir = Path.Combine(basePath,"..","..", "Binaries", "Win64");
			if (!Directory.Exists(binariesDir))
				System.IO.Directory.CreateDirectory(binariesDir);
 
			// Load lua libs
			LoadLua();
			string WW_DllDest = System.IO.Path.Combine(binariesDir, "downtown_dll.dll");
			CopyFile(DLLpath, WW_DllDest);

		// }
	}
    
    // A useful function from "parallelcube" to copy files...
    private void CopyFile(string source, string dest)
    {
        System.Console.WriteLine("Copying {0} to {1}", source, dest);
        if (System.IO.File.Exists(dest))
        {
            System.IO.File.SetAttributes(dest, System.IO.File.GetAttributes(dest) & ~System.IO.FileAttributes.ReadOnly);
        }
        try
        {
            System.IO.File.Copy(source, dest, true);
        }
        catch (System.Exception ex)
        {
            System.Console.WriteLine("Failed to copy file: {0}", ex.Message);
        }
    }

	// Used to load lua libs
	// private bool LoadLua(ReadOnlyTargetRules TargetRules){
	private bool LoadLua(){
		bool isLibSupported = false;

		if((Target.Platform == UnrealTargetPlatform.Win64) || (Target.Platform == UnrealTargetPlatform.Win32)) {
			isLibSupported = true;

			// Paths
			string PlatformString = (Target.Platform == UnrealTargetPlatform.Win64) ? "x64" : "x86";
			string LibrariesPath = Path.Combine(ThirdPartyPath(), "lua535", "libs");
			
			// Include .lib file
			PublicAdditionalLibraries.Add(Path.Combine(LibrariesPath, "lua53.lib"));
			// Location of include headers
			PublicIncludePaths.Add(Path.Combine(ThirdPartyPath(), "lua535", "include"));
		}
		return isLibSupported;
	}
}
