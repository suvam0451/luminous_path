// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

// System
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/SpringArmComponent.h"
#include "TimerManager.h"
#include "Camera/CameraComponent.h"

// WW_Dependencies plugin
#include "WW_Library.h"
#include "Dependencies/Public/Human_StateManager.h"

// Gameplay Ability System (Base)
#include "AbilitySystemInterface.h"

// Gameplay Ability System (Derived)
#include "Dependencies/Public/GAS/WW_Attributes.h"
#include "Dependencies/Public/GAS/WW_AbilitySysComp.h"

#include "WW_Player.generated.h"

UCLASS()
class DEPENDENCIES_API AWW_Player : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AWW_Player();

	/** If true we have initialized our abilities */
	UPROPERTY()
		int32 bAbilitiesInitialized;

	// Implementing Gameplay  Ability System
	UWW_AbilitySysComp* AbilitySystemComponent;
	UWW_Attributes* AttributeSet;
	// Exposes the GSC as a single BP node.
	UFUNCTION(BlueprintPure)
		UAbilitySystemComponent* GetAbilitySystemComponent() const override;


	// GameplayAbilities associated overrides
	// Called when our PlayerState is assigned.
	void OnRep_PlayerState() override;
	// Updates abilities
	void UpdateAbilityStack();

	UPROPERTY(EditAnywhere, Category = "Param")
		USkeletalMeshComponent* PlayerMesh;
	UPROPERTY(EditAnywhere, Category = "Param")
		UCameraComponent* _camera;
	UPROPERTY(EditAnywhere, Category = "Param")
		USpringArmComponent* SpringArmComp;

	UPROPERTY(EditAnywhere, Category = "Param")
		float CapsuleHeight = 90.0f;
	UPROPERTY(EditAnywhere, Category = "Param")
		float CapsuleRadius = 30.0f;
	UPROPERTY(EditAnywhere, Category = "Param")
		FVector CameraRelativeLocation = FVector(-39.56f, 1.75f, 64.f);


protected:
	EPlayerStates horizontalState, verticalState;
	FTimerHandle CamerablendSpace_THandle;
	FTimerDelegate CamerablendSpace_TDelegate;
private:
	APlayerController* ControllerRef;

	UHuman_StateManager* StateManagerRef;

#pragma region Overrides

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void PossessedBy(AController* NewController) override;
	virtual void OnConstruction(const FTransform& Transform) override;

	// Input mapping helpers
	template<EPlayerActions actionName, bool isPressed>
	void ActionMapper();
	template<int32 axisName>
	void AxisMapper(float Value);
	void ReactionMapper(EResponses in);

#pragma endregion

public:

#pragma region State Handling

	UPROPERTY()
		bool bCanJump = false;
	UPROPERTY()
		bool bCanWalk = false;
	UPROPERTY()
		bool bCanDash = false;

	//parameters
	UPROPERTY()
		float walkSpeed = 400.0f;
	UPROPERTY()
		float jogSpeed = 600.0f;
	UPROPERTY()
		float runSpeed = 1000.0f;
	UPROPERTY()
		float gravityScale = 1.5f;
	UPROPERTY()
		float dashDistance = 1000.0f;
	UPROPERTY()
		float dashDuration = 1.5f;

	//
	UPROPERTY()
		bool dashAvailable = true;
	UPROPERTY()
		bool bOnGround = true;
#pragma endregion

	FTimerHandle CameraShake_THandle;
	FTimerDelegate CameraShake_TDelegate;

	UFUNCTION()
		void CameraShakeTimerMap(int Index, bool timerKey);
	UFUNCTION()
		void CameraShakeMap(int Index, bool key);

	UFUNCTION()
		void CameraZoomManager(float value);

	// Two helper functions...
	UFUNCTION(BlueprintCallable)
		bool GiveEffect(TSubclassOf<UGameplayEffect> In);
	UFUNCTION(BlueprintCallable)
		bool GiveAbility(TSubclassOf<UGameplayAbility> In, int KeyIndex);

	// Two variables
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<UGameplayEffect> EffectToApply;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<UGameplayAbility> AbilityToApply;

#pragma region Gameplay Ability System

	// Called from AttributeSet. We handle this natively in C++ 
	virtual void HandleHealthChanged(float DeltaValue, const struct FGameplayTagContainer& EventTags);

#pragma endregion

};
