// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WW_Library.h"
#include "Dependencies/Public/WW_EngineOverrides.h"
#include "WW_PotionItem.generated.h"


// This class holds all the item types...
// Overridden from RPGItem data structure..

#pragma region Item types

UCLASS()
class DEPENDENCIES_API UWW_PotionItem : public URPGItem
{
	GENERATED_BODY()

public:
	UWW_PotionItem() {
		ItemType = UWW_AssetManager::PotionItemType;
	}
};

UCLASS()
class DEPENDENCIES_API UWW_SkillItem : public URPGItem
{
	GENERATED_BODY()

public:
	UWW_SkillItem() {
		ItemType = UWW_AssetManager::SkillItemType;
	}
};


UCLASS()
class DEPENDENCIES_API UWW_WeaponItem : public URPGItem
{
	GENERATED_BODY()

public:
	UWW_WeaponItem() {
		ItemType = UWW_AssetManager::WeaponItemType;
	}

	/** Weapon actor to spawn */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapon)
		TSubclassOf<AActor> WeaponActor;
};

#pragma endregion