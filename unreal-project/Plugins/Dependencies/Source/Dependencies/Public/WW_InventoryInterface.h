// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
// For Minimal API
#include "Net/UnrealNetwork.h"
// For UInterface
#include "Engine/Engine.h"

// RPG systems
#include "Dependencies/Public/WW_Library.h"
#include "WW_InventoryInterface.generated.h"

UINTERFACE(MinimalAPI, meta = (CannotImplementInterfaceInBlueprint))
class UWW_InventoryInterface : public UInterface
{
	GENERATED_BODY()
};

class DEPENDENCIES_API IWW_InventoryInterface
{
	GENERATED_BODY()

public:
	/* Signature (To be copied over and implemented)...
		----------------------------------------------------------------------------------
		// Implementation of IWW_InventoryInterface...
		virtual const TMap<URPGItem*, FRPGItemData>& GetInventoryDataMap() const override;
		virtual const TMap<FRPGItemSlot, URPGItem*>& GetSlottedItemMap() const override;
		virtual FOnInventoryItemChangedNative& GetInventoryItemChangedDelegate() override;
		virtual FOnSlottedItemChangedNative& GetSlottedItemChangedDelegate() override;
		virtual FOnInventoryLoadedNative& GetInventoryLoadedDelegate() override;
		---------------------------------------------------------------------------------- */

	// Some of these classes seem to be exchangeable

	/** Returns the map of items to data */
	virtual const TMap<URPGItem*, FWW_ItemData>& GetInventoryDataMap() const = 0;

	/** Returns the map of slots to items */
	virtual const TMap<FWW_ItemSlot, URPGItem*>& GetSlottedItemMap() const = 0;

	/** Gets the delegate for inventory item changes */
	virtual FOnInventoryItemChangedNative& GetInventoryItemChangedDelegate() = 0;

	/** Gets the delegate for inventory slot changes */
	virtual FOnSlottedItemChangedNative& GetSlottedItemChangedDelegate() = 0;

	/** Gets the delegate for when the inventory loads */
	virtual FOnInventoryLoadedNative& GetInventoryLoadedDelegate() = 0;
};
