// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "WW_Interfaces.generated.h"



#pragma region Pickup Interface

 // This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UWW_Pickup : public UInterface
{
	GENERATED_BODY()
};

class DEPENDENCIES_API IWW_Pickup
{
	GENERATED_BODY()

	// Signature (Copy Paste this in C++ files and implement)
	/* ---------------------------------------------------

		// Implementation of IWW_Pickup
		virtual void OnInteract(AActor* Caller) override;
		virtual void StartFocus() override;
		virtual void StartFocus() override;

	------------------------------------------------------ */

public:

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interaction")
		void OnInteract(AActor* Caller);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interaction")
		void StartFocus();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interaction")
		void EndFocus();
};

#pragma endregion


// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UWW_HUDInterface : public UInterface
{
	GENERATED_BODY()
};

class DEPENDENCIES_API IWW_HUDInterface
{
	GENERATED_BODY()

		// Signature (Copy Paste this in C++ files and implement)
		/* ---------------------------------------------------

			// Implementation of IWW_Pickup
			virtual void OnInteract(AActor* Caller) override;
			virtual void StartFocus() override;
			virtual void StartFocus() override;

		------------------------------------------------------ */

public:

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interaction")
		void TestFunction();
};