// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"

// Ability system implementation...
#include "AbilitySystemInterface.h"
#include "AbilitySystemComponent.h"

// Classes from Dependencies plugin
#include "Dependencies/Public/GAS/WW_Attributes.h"
#include "WW_SP_PlayerState.generated.h"


UCLASS()
class DEPENDENCIES_API AWW_SP_PlayerState : public APlayerState, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	AWW_SP_PlayerState();

	// Our ability system component.
	UPROPERTY(BlueprintReadOnly, Category = "Player State Base | Ability System Component")
		UAbilitySystemComponent* AbilitySystemComponent;
	
	// Our attribute set( Did not see a proper use case)...
	UPROPERTY(BlueprintReadOnly, Category = "Player State Base | Attribute Set")
		UWW_Attributes* AttributeSet;

	// This container holds all abilities the player holds...
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityStack")
		TArray<TSubclassOf<UGameplayAbility>> AbilityStack;

	// Helper function to get our ability system component (Used by the IAbilitySystemInterface).
	UFUNCTION(BlueprintPure, Category = "Player State Base | Ability System Component")
		UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	// Helper function that returns our attribute set.
	UFUNCTION(BlueprintPure, Category = "Player State Base | Attribute Set")
		UWW_Attributes* GetAttributeSet() const;

	// Gives our ability system component the designated abilities in slots 1 and 2.
	//UFUNCTION(BlueprintCallable, Server, Reliable, WithValidation, Category = "Player State Base | Ability System Component")
	UFUNCTION(BlueprintCallable, Category = "Player State Base | Ability System Component")
		void GiveAbilities();

	// Gives us the functionality to add abilities via BP. (These will need to be activated via "Try activate Ability by Class" in BP)
	//UFUNCTION(BlueprintCallable, Server, Reliable, WithValidation, Category = "Player State Base | Ability System Component")
	UFUNCTION(BlueprintCallable, Category = "Player State Base | Ability System Component")
		void GiveGameplayAbility(TSubclassOf<UGameplayAbility> AbilityToGive);

private:
	// Called when the game starts or when spawned.
	void BeginPlay() override;
};
