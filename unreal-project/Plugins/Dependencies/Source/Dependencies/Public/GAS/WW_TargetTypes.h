// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "AbilitySystemComponent.h"
#include "Dependencies/Public/WW_Player.h"
#include "Net/UnrealNetwork.h"
#include "WW_TargetTypes.generated.h"

// ------------------------------------------------------------
// Some Multiplayer and Blueprint features were stripped off... 
// ------------------------------------------------------------

/**
 * Class that is used to determine targeting for abilities
 * It is meant to be blueprinted to run target logic
 * This does not subclass GameplayAbilityTargetActor because this class is never instanced into the world
 * This can be used as a basis for a game-specific targeting blueprint
 * If your targeting is more complicated you may need to instance into the world once or as a pooled actor
 */
UCLASS()
class DEPENDENCIES_API UWW_TargetType : public UObject
{
	GENERATED_BODY()

public:
	UWW_TargetType() {};

	/** Called to determine targets to apply gameplay effects to */
	UFUNCTION(BlueprintNativeEvent)
	void GetTargets(AWW_Player* TargetingCharacter, AActor* TargetingActor, FGameplayEventData EventData, TArray<FHitResult>& OutHitResults, TArray<AActor*>& OutActors) const;
};

/** Trivial target type that uses the owner */
UCLASS(NotBlueprintable)
class DEPENDENCIES_API UWW_TargetType_UseOwner : public UWW_TargetType {
	
	GENERATED_BODY()

public:
	// Constructor and overrides
	UWW_TargetType_UseOwner() {}

	/** Uses the passed in event data */
	virtual void GetTargets_Implementation(AWW_Player* TargetingCharacter, AActor* TargetingActor, FGameplayEventData EventData, TArray<FHitResult>& OutHitResults, TArray<AActor*>& OutActors) const override;
};

/** Trivial target type that pulls the target out of the event data */
UCLASS(NotBlueprintable)
class DEPENDENCIES_API UWW_TargetType_UseEventData : public UWW_TargetType {

	GENERATED_BODY()

public:
	// Constructor and overrides
	UWW_TargetType_UseEventData() {}

	/** Uses the passed in event data */
	virtual void GetTargets_Implementation(AWW_Player* TargetingChar, AActor* target, FGameplayEventData EventData, TArray<FHitResult>& OutHitResults, TArray<AActor*>& OutActors) const override;
};