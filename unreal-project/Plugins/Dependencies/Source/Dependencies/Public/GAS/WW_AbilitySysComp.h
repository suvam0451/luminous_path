// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "WW_AbilitySysComp.generated.h"

/**
 * 
 */
UCLASS()
class DEPENDENCIES_API UWW_AbilitySysComp : public UAbilitySystemComponent
{
	GENERATED_BODY()
public:
	UWW_AbilitySysComp();

	/** Returns a list of currently active ability instances that match the tags */
	//void GetActiveAbilitiesWithTags(const FGameplayTagContainer& GameplayTagContainer, TArray<URPGGameplayAbility*>& ActiveAbilities);
	//
	/** Returns the default level used for ability activations, derived from the character */
	int32 GetDefaultAbilityLevel() const;
	
	/** Version of function in AbilitySystemGlobals that returns correct type */
	static UWW_AbilitySysComp* GetAbilitySystemComponentFromActor(const AActor* Actor, bool LookForComponent = false);
};
