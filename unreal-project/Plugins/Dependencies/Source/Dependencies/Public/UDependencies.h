// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Modules/ModuleInterface.h"
#include "Modules/ModuleManager.h"

class FDependencies : public IModuleInterface
{
public:
	void* DLLHandle;
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};