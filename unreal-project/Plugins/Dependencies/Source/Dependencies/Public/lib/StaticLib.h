// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
//#include "GameModes/ThirdP_UI.h"
#include "GameFramework/Actor.h"
#include "Engine/DataTable.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "Components/SplineMeshComponent.h"
#include "Components/SplineComponent.h"

#define loop(i,N) for(int i=0; i<N; i++)
#define setTimerFunc GetWorld()->GetTimerManager().SetTimer
//Quick setup for non ticking actors
#define WW_Constructor 	PrimaryActorTick.bCanEverTick = false; \
						SceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRoot")); \
						RootComponent = SceneRoot; \
						SceneRoot->SetMobility(EComponentMobility::Type::Static);

#define ISM UInstancedStaticMesh
#define ISMC UInstancedStaticMeshComponent
#define Spline_MC USplineMeshComponent
#define SMC UStaticMeshComponent
#define SM UStaticMesh

#define PATH "/WinterWildfire/_Main/_Assets"
#define PATH2 "/WinterWildfire/_Main/_Assets/"

UENUM(BlueprintType)
enum class EWW_SpawnerParameter : uint8
{
	Walls_DT UMETA(DisplayName = "Walls(DT)"),
	Stairs_Array UMETA(DisplayName = "Stair(int)"),
	Stairs_DT UMETA(DisplayName = "Stair(DT)"),
	Cables_Spline UMETA(DisplayName = "Cables(Spline)"),
	Grid_P UMETA(DisplayName = "2D Grid(int)"),
	Bridge_P UMETA(DisplayName = "Bridge(Spline,ISM)"),
};


class DEPENDENCIES_API WW_StaticLib
{
public:

	WW_StaticLib();
	~WW_StaticLib();

/* This section only useful if dynamically loading DLLs
	static void LoadDLL(FString folder, FString name, void* &handle)
	{
		FString filePath;
	#if WITH_EDITOR
		filePath = *FPaths::ProjectPluginsDir() + folder + "/" + name;
	#else
		filePath = *FPaths::ConvertRelativePathToFull(FPaths::RootDir()) + folder + "/" + name;
	#endif
		if (FPaths::FileExists(filePath))
		{
			//UE_LOG(LogTemp, Warning, TEXT("Yes file exists."));
			handle = NULL;
			handle = FPlatformProcess::GetDllHandle(*filePath); // Retrieve the DLL.
			if (handle != NULL)
			{
				UE_LOG(LogTemp, Warning, TEXT("DLL loads successfully"));
				return;
			}
			else {
				UE_LOG(LogTemp, Warning, TEXT("Handle not obtained. Why??"));
			}
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("No, file does not exist."));
		}
		UE_LOG(LogTemp, Warning, TEXT("Fuck. DLL does not load successfully"));
	}
	
	template <class any>
	static void getExported(void* &handle, FString name, any &func) {
		if (handle != NULL) {
			func = NULL;
			func = (any)FPlatformProcess::GetDllExport(handle, *name);
			if (func != NULL) {
				return;
			}
		}
		return;
	}
	static void FreeDLL(void* handle) {
		if (handle != NULL)
		{
			FPlatformProcess::FreeDllHandle(handle);
			handle = NULL;
		}
	}*/

	//Cleaning duty for elements in actor by type...
	template <class any, typename type>
	static void cleanByType(any* reference) {
		while (reference->GetComponentByClass(type::StaticClass()) != NULL) {
			UActorComponent* cls = reference->GetComponentByClass(type::StaticClass());
			//Unregister automatically called
			//cls->UnregisterComponent();
			cls->DestroyComponent();
			cls->SetActive(false);
		}
		return;
	}

	//Cleaning duty for array references...
	template <class any>
	static void cleanArray(TArray<any*> &reference) {
		for (int i = 0; i < reference.Num(); i++) {
			if (IsValid(reference[i]) && reference[i] != NULL) {
				reference[i]->Destroy();
			}
		}
		reference.Empty();
	}

	/*Cleans actors in any map container*/
	template <class any>
	static void cleanMap(TMap<int, any*>& ref) {
		for (int i = 0; i < ref.Num(); i++) {
			if (IsValid(ref[i + 1]) && ref[i + 1] != NULL) {
				//reference[i]->Destroy();
				ref[i + 1]->DestroyComponent();
			}
		}
		ref.Empty();
	}
	//template<class any>
	//static void ISM_Initialize(ISMC* ref, AActor* parent, USceneComponent* SceneRoot) {
	/*  @ Cleans remaining traces of instanced meshes
		@ Emptys container and re-initializes it
	*/
	static void ISM_RefreshByNumber(TMap<int, ISMC*> &ref, int in, AActor* parent, USceneComponent* SceneRoot) {
		WW_StaticLib::cleanByType<AActor, ISMC>(parent);
		WW_StaticLib::cleanByType<AActor, SMC>(parent);
		ref.Empty();

		loop(i, in) {
			ref.Add(i+1, NewObject<ISMC>(parent));
		}
		for (auto& it : ref) {
			it.Value->SetFlags(RF_Transactional);
			it.Value->CreationMethod = EComponentCreationMethod::UserConstructionScript;
			it.Value->SetMobility(EComponentMobility::Type::Static);
			it.Value->IsRegistered() ? true : it.Value->RegisterComponent();
			it.Value->AttachToComponent(SceneRoot, FAttachmentTransformRules(EAttachmentRule::KeepRelative, true));
			parent->AddInstanceComponent(it.Value);
		}
	}

	static void SplineMap_RefreshByNumber(TMap<int, USplineComponent*>& ref, int in, AActor* parent, USceneComponent* SceneRoot) {
		//WW_StaticLib::cleanByType<AActor, USplineComponent>(parent);
		//WW_StaticLib::cleanByType<AActor, USplineMeshComponent>(parent);
		ref.Empty();

		loop(i, in) {
			ref.Add(i + 1, NewObject<USplineComponent>(parent));
		}
		for (auto& it : ref) {
			it.Value->SetFlags(RF_Transactional);
			it.Value->CreationMethod = EComponentCreationMethod::UserConstructionScript;
			it.Value->SetMobility(EComponentMobility::Type::Static);
			it.Value->IsRegistered() ? true : it.Value->RegisterComponent();
			it.Value->AttachToComponent(SceneRoot, FAttachmentTransformRules(EAttachmentRule::KeepRelative, true));
			parent->AddInstanceComponent(it.Value);
		}
	}

	//any -> A subclass deriving from AActor :)
	template <class any>
	static void ISM_Initialize(ISMC* ref, any* parent, USceneComponent* SceneRoot, SM* sm = NULL) {
		ref->SetFlags(RF_Transactional);
		ref->CreationMethod = EComponentCreationMethod::UserConstructionScript;
		ref->SetMobility(EComponentMobility::Type::Static);
		ref->SetStaticMesh(sm);
		ref->IsRegistered() ? true : ref->RegisterComponent();
		ref->AttachToComponent(SceneRoot, FAttachmentTransformRules(EAttachmentRule::KeepRelative, true));
		parent->AddInstanceComponent(ref);
	}

	//any -> A subclass deriving from AActor :)
	template <class any>
	static Spline_MC* Spline_MC_Init(any* parent, USceneComponent* SceneRoot, SM* sm = NULL) {	
		Spline_MC* ref = NewObject<USplineMeshComponent>(parent);
		ref->CreationMethod = EComponentCreationMethod::UserConstructionScript;
		ref->SetMobility(EComponentMobility::Type::Static);
		ref->SetForwardAxis(ESplineMeshAxis::X);
		ref->SetStaticMesh(sm);
		ref->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);
		ref->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		ref->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
		ref->AttachToComponent(SceneRoot, FAttachmentTransformRules(EAttachmentRule::KeepRelative, true));
		ref->RegisterComponent();
		parent->AddInstanceComponent(ref);
		return ref;
	}
};

