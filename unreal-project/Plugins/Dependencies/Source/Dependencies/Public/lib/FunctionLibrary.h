#pragma once  

#define WINTERWILDFIRE_API __declspec(dllexport)	//shortens __declspec(dllexport) to DLL_EXPORT

// Useful macros usable in any engine
#define loop(i,N) for(int i = 0; i < N; i++)

/*
	@ Following are UE4 specific macros to make life easier for a C++ programmer.
	@ -- Actor initializations
	@ -- Mini algo macros like loops
*/
#define UE4_StaticActorInit PrimaryActorTick.bCanEverTick = false; \
							SceneRoot->SetMobility(EComponentMobility::Type::Static);



#ifdef __cplusplus		//if C++ is used convert it to C to prevent C++'s name mangling of method names


class string;

extern "C++"
{
#endif
	enum WINTERWILDFIRE_API States {
		Walking,
		Grabbing,
		Running,
		Flying,

		Jumping,
		Falling,
		Hanging,
		Grounded,
		Floating,
		Dashing
	};
	enum WINTERWILDFIRE_API Responses {
		DoNothing,
		Sprint,
		Jump,
		StartDashing,
		DashNotAvailable
	};

	struct WINTERWILDFIRE_API Vector3 {
		float x;
		float y;
		float z;
	};
	bool WINTERWILDFIRE_API WWdashAvailable = false;
	bool WINTERWILDFIRE_API WWOnGround = false;
	bool WINTERWILDFIRE_API WWCanMoveMoveAhead = true;
	bool WINTERWILDFIRE_API WWCanMoveSideways = true;
	bool WINTERWILDFIRE_API references[4];
	//bool* DLL_EXPORT dragon[4];

	int WINTERWILDFIRE_API sendStateChangeRequest(int initial, int final);
	void WINTERWILDFIRE_API sendStateUpdate(int* array, int N);
	void WINTERWILDFIRE_API InitializeDLL();

	void WINTERWILDFIRE_API returnVectorList(Vector3 in, Vector3* &ass);

	int WINTERWILDFIRE_API testFunction(int myarray);

	/*
		Regex matches string. Parameters: flag = 0
		@ flag -1    :  Match string as is fully             =>                ** ignores nothing **
		@ flag 0    :  Match string as is fully             =>                ** compares T_ from T_YourTexture_01 **
		@ flag 1    :  Ignore type identifier               =>                ** ignores YourTexture from T_YourTexture_01 **
		@ flag 2    :  Ignores type identifier and name     =>                ** ignores _01 from T_YourTexture_01 ** (Index as string is auto formatted)
	*/
	bool WINTERWILDFIRE_API RegexMatchSegment(std::string in, std::string with, int flag = 0);
	bool WINTERWILDFIRE_API RegexTestFunction(std::string in, std::string with, int flag = 0);
#ifdef __cplusplus


}
#endif