// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "lua.hpp"
//#include "lua.hpp"
// #include "ThirdParty/lua"
#include "LuaActor.generated.h"

UCLASS()
class DEPENDENCIES_API ALuaActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALuaActor();

	// Lua script source file (Unused unless prompted)
	UPROPERTY(EditAnywhere, Category = "Lua Scripting")
		FString LuaSource;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Calls StartDesigner() and sets bForceOneUpdate to false.
	virtual void OnConstruction(const FTransform& Transform) override;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	bool CheckLua(lua_State* L, int r);

	// Our own scene component, because why not ?
	USceneComponent* SceneRoot;
};
