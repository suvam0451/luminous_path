// Copyright ‎© 2019 Debashish Patra, released under Mozilla Public License, version 2.0.
// See file LICENSE.md for full license details.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Internationalization/Regex.h"
#include "Styling/SlateBrush.h"

// Engine modules
#include "AssetRegistry/Private/AssetRegistry.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "Runtime/Core/Public/Async/ParallelFor.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Runtime/AssetRegistry/Public/AssetRegistryModule.h"
//#include "WW_Library.h"

// Used by inventory system
#include "UObject/PrimaryAssetId.h"
#include "Engine/DataAsset.h"

// GameInstance equivalent
#include "Dependencies/Public/WW_GISubsystem.h"
#include "WW_Library.generated.h"



#pragma region Gameplay Abilities

// Key bindings
UENUM(BlueprintType)
enum class AbilityInput : uint8
{
	Ability1	UMETA(DisplayName = "Ability1"),	// This will be bound via key bindings in engine as "Ability1"
	Ability2	UMETA(DisplayName = "Ability2"),	// This will be bound via key bindings in engine as "Ability2"
	Ability3	UMETA(DisplayName = "Ability3"),	// This will be bound via key bindings in engine as "Ability3"
	Ability4	UMETA(DisplayName = "Ability4"),	// This will be bound via key bindings in engine as "Ability1"
	Ability5	UMETA(DisplayName = "Ability5"),	// This will be bound via key bindings in engine as "Ability2"
	Ability6	UMETA(DisplayName = "Ability6"),	// This will be bound via key bindings in engine as "Ability3"
	Ability7	UMETA(DisplayName = "Ability7"),	// This will be bound via key bindings in engine as "Ability1"
	Ability8	UMETA(DisplayName = "Ability8"),	// This will be bound via key bindings in engine as "Ability2"
	Ability9	UMETA(DisplayName = "Ability9"),	// This will be bound via key bindings in engine as "Ability3"
};

#pragma endregion

#pragma region RPG Items

	// Your custom ability class goes here
	class UWW_GameplayAbility;
	/** Base class for all items, do not blueprint directly */
	UCLASS(Abstract, BlueprintType)
		class DEPENDENCIES_API URPGItem : public UPrimaryDataAsset
	{
		GENERATED_BODY()

	public:
		/** Constructor */
		URPGItem() : Price(0), MaxCount(1), MaxLevel(1), AbilityLevel(1) {}

		/** Type of this item, set in native parent class */
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Item)
			FPrimaryAssetType ItemType;

		/** User-visible short name */
		UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Item)
			FText ItemName;

		/** User-visible long description */
		UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Item)
			FText ItemDescription;

		/** Icon to display */
		UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Item)
			FSlateBrush ItemIcon;

		/** Price in game */
		UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Item)
			int32 Price;

		/** Maximum number of instances that can be in inventory at once, <= 0 means infinite */
		UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Max)
			int32 MaxCount;

		/** Returns if the item is consumable (MaxCount <= 0)*/
		UFUNCTION(BlueprintCallable, BlueprintPure, Category = Max)
			bool IsConsumable() const {
				return	(MaxCount <=0 );
		};

		/** Maximum level this item can be, <= 0 means infinite */
		UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Max)
			int32 MaxLevel;

		/** Ability to grant if this item is slotted */
		UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Abilities)
			TSubclassOf<UWW_GameplayAbility> GrantedAbility;

		/* Ability level to grant */
		UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Abilities)
			int32 AbilityLevel;

		/** Returns the logical name, equivalent to the primary asset id */
		UFUNCTION(BlueprintCallable, Category = Item)
			FString GetIdentifierString() const {
				return GetPrimaryAssetId().ToString();
		};

		/** Overridden to use saved type */
		virtual FPrimaryAssetId GetPrimaryAssetId() const override {
			// This is a DataAsset and not a blueprint so we can just use the raw FName
			// For blueprints you need to handle stripping the _C suffix
			return FPrimaryAssetId(ItemType, GetFName());
		};
	};

#pragma endregion

#pragma region RPG Inventory structs

// Subclass this for inventory items...
// class URPGItem;

/** Struct representing a slot for an item, shown in the UI */
USTRUCT(BlueprintType)
struct DEPENDENCIES_API FWW_ItemSlot {

	GENERATED_BODY()

		/** Constuctor, -1 means invalid slot */
		FWW_ItemSlot() : SlotNumber(-1) {};

	FWW_ItemSlot(const FPrimaryAssetType& InItemType, int32 InSlotNumber) : ItemType(InItemType), SlotNumber(InSlotNumber) {}

	/** The type of items that can go in this slot */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Item)
		FPrimaryAssetType ItemType;

	/** The number of this slot, 0 indexed */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Item)
		int32 SlotNumber;

	/** Equality operators */
	bool operator==(const FWW_ItemSlot& Other) const
	{
		return ItemType == Other.ItemType && SlotNumber == Other.SlotNumber;
	}
	bool operator!=(const FWW_ItemSlot& Other) const
	{
		return !(*this == Other);
	}

	/** Implemented so it can be used in Maps/Sets */
	friend inline uint32 GetTypeHash(const FWW_ItemSlot& Key)
	{
		uint32 Hash = 0;

		Hash = HashCombine(Hash, GetTypeHash(Key.ItemType));
		Hash = HashCombine(Hash, (uint32)Key.SlotNumber);
		return Hash;
	}

	/** Returns true if slot is valid */
	bool IsValid() const
	{
		return ItemType.IsValid() && SlotNumber >= 0;
	}
};

/** Extra information about a URPGItem that is in a player's inventory */
USTRUCT(BlueprintType)
struct DEPENDENCIES_API FWW_ItemData {

	GENERATED_BODY()

	// Constructors
	FWW_ItemData() : ItemCount(1), ItemLevel(1) {}
	FWW_ItemData(int32 InItemCount, int32 InItemLevel): ItemCount(InItemCount), ItemLevel(InItemLevel) {}

	/** The number of instances of this item in the inventory, can never be below 1 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Item)
		int32 ItemCount;

	/** The level of this item. This level is shared for all instances, can never be below 1 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Item)
		int32 ItemLevel;

	/* Operator Overrides */
	bool operator==(const FWW_ItemData& Other) const {
		return ItemCount == Other.ItemCount && ItemLevel == Other.ItemLevel;
	}
	bool operator!=(const FWW_ItemData& Other) const {
		return !(*this == Other);
	}

	/** Returns true if count is greater than 0 */
	bool IsValid() const {
		return ItemCount > 0;
	}

	/** Append an item data, this adds the count and overrides everything else */
	void UpdateItemData(const FWW_ItemData& Other, int32 MaxCount, int32 MaxLevel) {
		if (MaxCount <= 0)
		{
			MaxCount = MAX_int32;
		}

		if (MaxLevel <= 0)
		{
			MaxLevel = MAX_int32;
		}

		ItemCount = FMath::Clamp(ItemCount + Other.ItemCount, 1, MaxCount);
		ItemLevel = FMath::Clamp(Other.ItemLevel, 1, MaxLevel);
	}
};

/** Delegate called when an inventory item changes */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnInventoryItemChanged, bool, bAdded, URPGItem*, Item);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnInventoryItemChangedNative, bool, URPGItem*);

/** Delegate called when the contents of an inventory slot change */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnSlottedItemChanged, FWW_ItemSlot, ItemSlot, URPGItem*, Item);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnSlottedItemChangedNative, FWW_ItemSlot, URPGItem*);

/** Delegate called when the entire inventory has been loaded, all items may have been replaced */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnInventoryLoaded);
DECLARE_MULTICAST_DELEGATE(FOnInventoryLoadedNative);

#pragma endregion

#pragma region Utility functions
	
class DEPENDENCIES_API WW_UtilityHelper
{
//public:
};
#pragma endregion


#pragma region Regex Helpers

class DEPENDENCIES_API WW_RegexHelper
{

public:
	/* Simplest regex query */
	static bool SimpleRegexQuery(FString In, FString regex);

	/* Simple regex query. Also returns indices of first match */
	static bool SimpleRegexQuery(FString In, FString regex, TPair<int32, int32>& retval);


	/* Function to remove filename portion from full length package name...
		@ e.g. Input - /ScriptableActors/_Main/_Cached/RoadSample/SM_RoadBase_01
		@ e,g. File  - SM_RoadBase_01
		@ Output     - /ScriptableActors/_Main/_Cached/RoadSample/
	*/
	static FString RegexExtractDirectory(FString pathname, FString filename);

	/* Will return "Name" from "SM_Name_01" */
	static FString GetAssetName(FString In);

	/* Gets the asset name as per UE4 naming conventions
		@ UE4 convention --> SM_NameOfMesh_01_ExtraData
	*/
	static FString GetAssetName(FString evaluate, FString regex);
};

#pragma endregion

#pragma region AssetRegistry Helpers

class DEPENDENCIES_API WW_AssetRegistryHelper
{
public:
	/*
		Scans the registry for a "type"
		@ returns : An array of all elemnts by type;
		---- Inputs ----
		@ location : Location in folder structure to scan
		@ regex : The regex to use. This is applied after type specified finds array of object
		@ in : templated array expected to be made in calling program
		@ consoleDebug(optional) :  
	/**/
	template<class returntype>
	static bool ScanForRegexByType(FString location, FString regex, returntype* &in, bool bConsoleDebug = false) {
		FAssetRegistryModule* AssetRegistryModule = &FModuleManager::LoadModuleChecked<FAssetRegistryModule>(TEXT("AssetRegistry"));
		IAssetRegistry& AssetRegistry = AssetRegistryModule->Get();
		TArray<FAssetData> retval;

		// Setup Filter...
		FARFilter Filter;
		Filter.bRecursivePaths = false;
		Filter.PackagePaths.Add(FName(*location));
		Filter.ClassNames.Add(returntype::StaticClass()->GetFName());
		AssetRegistry.GetAssets(Filter, retval);
		//retval[0].path

		// Log message section...
		if (bConsoleDebug == true) { UE_LOG(LogTemp, Warning, TEXT("AssetRegistry : %d assets found by type in %s"), retval.Num(),*location); }

		for (auto it : retval) {
			if(WW_RegexHelper::SimpleRegexQuery(it.AssetName.ToString(), regex)) {
				in = it.IsValid() ? Cast<returntype>(it.GetAsset()) : nullptr;
			}
			return true;
		}
		return false;
	}
};

#pragma endregion

#pragma region ConsoleLog Helpers

class DEPENDENCIES_API WW_cout {
public:
	static void Error(int code, FString info = "None"){
		FString retval;
		switch(code){
			case 404: retval = (info=="None") ? "Something is not found" : info + "is not found"; break;
			default: break;
		}
	}
};

#pragma endregion
