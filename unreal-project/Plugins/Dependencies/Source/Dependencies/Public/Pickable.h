// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WW_Interfaces.h"
#include "Pickable.generated.h"

UCLASS()
class DEPENDENCIES_API APickable : public AActor, public IWW_Pickup
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickable();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Implement IWW_Pickup
	void OnInteract(AActor* Caller);
	void StartFocus();
	void EndFocus();
};
