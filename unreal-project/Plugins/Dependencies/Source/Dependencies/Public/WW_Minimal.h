#pragma once

#include "CoreMinimal.h"
#include "WW_Library.h"

// Includes for material nodes
#ifndef WW_MATERIALNODE
#define WW_MATERIALNODE 1
#endif

#if WW_MATERIALNODE
	#include "MaterialExpressionIO.h"
	#include "Materials/MaterialExpression.h"
	#include "MaterialCompiler.h"
	#include "Runtime/Engine/Classes/Materials/MaterialExpressionMultiply.h"
	#include "Runtime/Engine/Classes/Materials/MaterialExpressionConstant.h"
	#include "Runtime/Engine/Classes/Materials/MaterialExpressionComponentMask.h"
	#include "Materials/MaterialExpressionConstant2Vector.h"
	#include "Materials/MaterialExpressionConstant3Vector.h"
	#include "Materials/MaterialExpressionMakeMaterialAttributes.h"
	#include "Materials/MaterialExpressionAdd.h"
	#include "Runtime/Engine/Private/Materials/MaterialUniformExpressions.h"

	#include "Materials/MaterialExpressionMultiply.h"
#endif

#define loop(i,N) for(int i =0;i<N;i++)
#define WW_StaticActor 	PrimaryActorTick.bCanEverTick = false; \
						SceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRoot")); \
						RootComponent = SceneRoot; \
						SceneRoot->SetMobility(EComponentMobility::Type::Static);