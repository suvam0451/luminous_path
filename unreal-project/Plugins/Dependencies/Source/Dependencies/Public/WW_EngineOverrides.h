// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
// Need engine to be overridden
#include "EngineMinimal.h"
#include "Engine/Engine.h"
#include "Engine/AssetManager.h"
#include "Dependencies/Public/WW_Library.h"
#include "WW_EngineOverrides.generated.h"

/**
	This class holds modifications of Engine Classes that are important for operating the plug-in.
	If you would like to extend it for your own game, please refer to our online documentation.
	The process should be modular with few copy-pastes. List of overrides :

	@        Name          |     Base       |     Warnings      |      Reason        |
	@ ---------------------|----------------|-------------------|--------------------|
	@   UWW_AssetManager   | AssetManager   |      None         | better GAS system  |

 */
UCLASS()
class DEPENDENCIES_API UWW_AssetManager : public UAssetManager
{
	GENERATED_BODY()
public:

	UWW_AssetManager() {};
	virtual void StartInitialLoading() override;

	// These need to be defined in source file. Why ? Well, somehow...
	static const FPrimaryAssetType PotionItemType;
	static const FPrimaryAssetType SkillItemType;
	static const FPrimaryAssetType WeaponItemType;


	/** Returns the current AssetManager object */
	static UWW_AssetManager& Get();

	/**
	* Synchronously loads an RPGItem subclass, this can hitch but is useful when you cannot wait for an async load
	* This does not maintain a reference to the item so it will garbage collect if not loaded some other way
	*
	* @param PrimaryAssetId The asset identifier to load
	* @param bDisplayWarning If true, this will log a warning if the item failed to load
	*/
	URPGItem* ForceLoadItem(const FPrimaryAssetId& PrimaryAssetId, bool bLogWarning = true);
};
