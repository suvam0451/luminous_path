// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#define WW_MATERIALNODE 1

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "Engine/StreamableManager.h"
#include "WW_Minimal.h"
#include "WW_GISubsystem.generated.h"

/**
 * 
 */
UCLASS()
class DEPENDENCIES_API UWW_GISubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
public:
	UWW_GISubsystem();
	void PrintFunction();
	// Super classes to be overridden
	// virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	// virtual void Deinitialize() override;

	// UGameInstance* GI = World ? World->GetGameInstance() : nullptr;
	// return UGameInstance::GetSubsystem<UActionsSubsystem>(GI);

	FStreamableManager AssetLoader;

	//TArray< TAssetPtr<ABaseItem> > MyItems;
};

#undef WW_MATERIALNODE