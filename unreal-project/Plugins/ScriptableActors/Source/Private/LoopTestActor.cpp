// Fill out your copyright notice in the Description page of Project Settings.

#include "LoopTestActor.h"

// Sets default values
ALoopTestActor::ALoopTestActor()
{
	//WW_Constructor;
	//FStringAssetReference AssetToLoad;
	//FStreamableManager AssetLoader;
	TheMainManHolder = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh holder"));
	TheMainManHolder->SetupAttachment(SceneRoot);
}

void ALoopTestActor::OnConstruction(const FTransform& Transform)
{
	StartDesigner();
}

void ALoopTestActor::StartDesigner() {

#if WITH_EDITORONLY_DATA
	OnAssetLoaded();
#else
	RequestAssetLoading();
#endif

}

void ALoopTestActor::BeginPlay()
{
	Super::BeginPlay();

}

void ALoopTestActor::RequestAssetLoading() {
	UGameInstance* GameInstance = GetGameInstance();
	UWW_GISubsystem* MySubsystem = GameInstance->GetSubsystem<UWW_GISubsystem>();

	TArray<FStringAssetReference> AssetList;
	AssetList.AddUnique(TheMainMan.ToSoftObjectPath());

	MySubsystem->AssetLoader.RequestAsyncLoad(AssetList, FStreamableDelegate::CreateUObject(this, &ALoopTestActor::OnAssetLoaded));
}

void ALoopTestActor::OnAssetLoaded() {
	// Null check for static mesh. If valid, get and proceed...
	if(!TheMainMan.IsValid()) {
		WW_cout::Error(404, "One of the SM_");
		return;
	}
	else {
		TheMainManHolder->SetStaticMesh(TheMainMan.Get());
	}

	UStaticMesh* TempMesh = TheMainMan.Get();
	// Extract extra classes/assets for sampled mesh using location data...
	FString AssetName = WW_RegexHelper::GetAssetName(*TheMainMan.GetAssetName(), "^(SM_)+(.*?)");

	FString newregex = "^(T_)+" + AssetName + "+(.*?)";
	FString dir = WW_RegexHelper::RegexExtractDirectory(TheMainMan.GetLongPackageName(), TheMainMan.GetAssetName());
	UTexture2D* InTexture;
	UE_LOG(LogTemp, Warning, TEXT("file name %s %i"), *dir, 1);
	WW_AssetRegistryHelper::ScanForRegexByType<UTexture2D>(dir, newregex, InTexture);

	if(InTexture == NULL || TheMainManHolder == NULL) {
		WW_cout::Error(404, "Texture for " + AssetName);
	}
	else {
		DynMaterial = UMaterialInstanceDynamic::Create(Material, this);	
		DynMaterial->SetTextureParameterValue("Intexture", InTexture);
		TheMainManHolder->SetMaterial(0, DynMaterial);
			//TempMesh->
	}
	if(RetainTextures) {
		// TheMainManHolder->SetMaterial(0, );  
	}
}
// Called every frame
void ALoopTestActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


void ALoopTestActor::ALongAndArduousTask(int howmany){
	for (int32 i = 1; i <= howmany; i++)
		{
			bool isPrime = true;
 
			for (int32 j = 2; j <= i / 2; j++)
			{
				if (FMath::Fmod(i, j) == 0)
				{
					isPrime = false;
					break;
				}
			}
 
			if (isPrime) GLog->Log("Prime number #" + FString::FromInt(i) + ": " + FString::FromInt(i));
		}
}

void ALoopTestActor::ALongAndArduousTaskAsync(int howmany) {
	(new FAutoDeleteAsyncTask<FPrimeSearchTask>(howmany))->StartBackgroundTask();
}