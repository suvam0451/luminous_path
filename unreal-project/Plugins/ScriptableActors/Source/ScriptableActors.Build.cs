// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.IO;

public class ScriptableActors : ModuleRules
{
	public ScriptableActors(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		// PCHUsage = PCHUsageMode.NoSharedPCHs;
		// PrivatePCHHeaderFile = "Public/WW_PCH.h";

		PublicDependencyModuleNames.AddRange(
			new string[] {
				"Core", "UMG", "Dependencies"
			});
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[] {
				"CoreUObject", "Engine", "Slate", "SlateCore"	
			});
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[] {});
		
		PrivateIncludePaths.AddRange(
			new string[] {
				"Private",
			});
	}
}
