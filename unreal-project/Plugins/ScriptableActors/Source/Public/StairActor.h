// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ScriptableParent.h"
#include "Misc/Paths.h"
#include "StairActor.generated.h"

/** Struct representing a slot for an item, shown in the UI */
// USTRUCT(BlueprintType)
// struct DEPENDENCIES_API FWW_StairPresets {
// 
// 	GENERATED_BODY()
// 
// 	/* Constuctor, -1 means invalid slot */
// 	FWW_StairPresets() : SlotNumber(-1) {};
// 
// 	//UPROPERTY(EditAnywhere)
// 	//	TArray<TSoftObjectPtr<>> 
// 	//FWW_ItemSlot(const FPrimaryAssetType& InItemType, int32 InSlotNumber) : ItemType(InItemType), SlotNumber(InSlotNumber) {}
// 
// 	/** The type of items that can go in this slot */
// 	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Item)
// 		FPrimaryAssetType ItemType;
// 
// 	/** The number of this slot, 0 indexed */
// 	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Item)
// 		int32 SlotNumber;
// 
// 	/** Equality operators */
// 	//bool operator==(const FWW_ItemSlot& Other) const
// 	//{
// 	//	return ItemType == Other.ItemType && SlotNumber == Other.SlotNumber;
// 	//}
// 	//bool operator!=(const FWW_ItemSlot& Other) const
// 	//{
// 	//	return !(*this == Other);
// 	//}
// 
// 	/** Implemented so it can be used in Maps/Sets */
// 	friend inline uint32 GetTypeHash(const FWW_StairPresets& Key)
// 	{
// 		uint32 Hash = 0;
// 
// 		Hash = HashCombine(Hash, GetTypeHash(Key.ItemType));
// 		Hash = HashCombine(Hash, (uint32)Key.SlotNumber);
// 		return Hash;
// 	}
// 
// 	/** Returns true if slot is valid */
// 	bool IsValid() const
// 	{
// 		return ItemType.IsValid() && SlotNumber >= 0;
// 	}
// };

UCLASS()
class SCRIPTABLEACTORS_API AStairActor : public AScriptableParent
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AStairActor();

	// Number of stairs for default operation mode
	UPROPERTY(EditAnywhere, Category = "Scriptables Extended")
		int StairNum;

	// Boolean paramters
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Scriptable Extended")
		bool Use_Base = true;
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Scriptable Extended")
		bool Use_Pole = true;
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Scriptable Extended")
		bool Use_Railing = true;
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Scriptable Extended")
		bool Use_Left = true;
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Scriptable Extended")
		bool Use_Right = true;

private:

	// override used functions only
	virtual void StartDesigner() override;

	// private tables/rows/references assets
	FStair_Presets* _presetRow;
	FStair_Script* _scriptRow;


	// ------------------------------ //
	// Actor specific implementations //
	// ------------------------------ //
	
	// Other actor-specific functions
	virtual void DefaultMode() override;
	virtual void ScriptedMode() override;

	// Other actor-specific assets
	UStaticMesh* DefaultRoadMesh = nullptr;

	// Other actor-specific containers
	TMap<int, FString> StringTable;
};
