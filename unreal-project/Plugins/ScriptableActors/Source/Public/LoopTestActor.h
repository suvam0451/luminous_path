// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Dependencies/Public/WW_Library.h"
#include "LoopTestActor.generated.h"

UCLASS()
class ALoopTestActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALoopTestActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UMaterialInstanceDynamic* DynMaterial;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

/* Main actor pipelines
	@ StartDesigner acts as pre-processor and accessible from everywhere.
	@ RequestAsyncLoading makes request for async loads
	@ OnAssetLoaded gets called in every case.
*/
UFUNCTION(BlueprintCallable, Category="SunShine")
	void StartDesigner();
UFUNCTION()
	void RequestAssetLoading();
UFUNCTION()
	void OnAssetLoaded();


UPROPERTY(EditAnywhere)
	TAssetPtr<UStaticMesh> TheMainMan;
UPROPERTY(EditAnywhere)
    UMaterialInterface* Material;
// UPROPERTY(EditAnywhere)
//   UTexture2D* InTexture;

// Allow users to retain textures in case of complex multi-textures...
UPROPERTY(EditAnywhere, AdvancedDisplay)
    bool RetainTextures = false;

UFUNCTION(BlueprintCallable, Category="SunShine")
	static void ALongAndArduousTask(int howmany);

UFUNCTION(BlueprintCallable, Category="SunShine")
	static void ALongAndArduousTaskAsync(int howmany);

	virtual void OnConstruction(const FTransform& Transform) override;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* TheMainManHolder;

private:
	UPROPERTY()
		USceneComponent* SceneRoot;
};

class FPrimeSearchTask : public FNonAbandonableTask
{
	friend class FAutoDeleteAsyncTask<FPrimeSearchTask>;

public:
	FPrimeSearchTask(int32 Input): MyInput(Input){
		this->MyInput = Input;
	}

protected:
	int32 MyInput;

	void DoWork() {
		ALoopTestActor::ALongAndArduousTask(MyInput);
		// Work to be pasted here
		// Called as follow
		// (new FAutoDeleteAsyncTask<FPrimeSearchTask>(6.10))->StartBackgroundTask();
	}

	// Absolutely no idea why this should be here...
	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FMyTaskName, STATGROUP_ThreadPoolAsyncTasks);
	}
};