// Fill out your copyright notice in the Description page of Project Settings.

#include "BTTask_MoveToPlayer.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
//#include "AIEssentialsCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "EnemyCharacter.h"
#include "EnemyAI.h"
//#include "Dependencies/Public/WW_Player.h"
#include "Dependencies/Public/WW_Player.h"


EBTNodeResult::Type UBTTask_MoveToPlayer::ExecuteTask(UBehaviorTreeComponent &OwnerComp, uint8* NodeMemory) {
    AEnemyAI *CharPC = Cast<AEnemyAI>(OwnerComp.GetAIOwner());

    // Replace 'return-type' of 'Enemy' for your player class here...
    AWW_Player *Enemy = Cast<AWW_Player>(OwnerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Object>(CharPC->EnemyKeyID));


    // If player found, execute action and return 'success'...
    Enemy ? CharPC->MoveToActor(Enemy, 5.0f, true, true, true, 0 , true) : true;
    if (Enemy) { UE_LOG(LogTemp, Warning, TEXT("Moving on...")); }
    else { UE_LOG(LogTemp, Warning, TEXT("Cant move on...")); }
    return Enemy ? EBTNodeResult::Succeeded : EBTNodeResult::Failed;
}