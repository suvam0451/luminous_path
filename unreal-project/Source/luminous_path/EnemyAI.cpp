// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAI.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "EnemyCharacter.h"

AEnemyAI::AEnemyAI() {
    BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("Blackboard Component"));
    BehaviorComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorTree Component"));
}

void AEnemyAI::OnPossess(APawn* InPawn) {
    Super::OnPossess(InPawn);

    AEnemyCharacter *Char = Cast<AEnemyCharacter>(InPawn);

    if (Char && Char->BotBehavior){
        BlackboardComp->InitializeBlackboard(*Char->BotBehavior->BlackboardAsset);

        EnemyKeyID = BlackboardComp->GetKeyID("Target");

        BehaviorComp->StartTree(*Char->BotBehavior);
    }
}