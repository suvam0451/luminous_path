// Fill out your copyright notice in the Description page of Project Settings.

#include "BTService_CheckForPlayer.h"
#include "CoreMinimal.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
//#include "AIEssentialsCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "EnemyCharacter.h"
#include "EnemyAI.h"


UBTService_CheckForPlayer::UBTService_CheckForPlayer(){
    bCreateNodeInstance = true;
}
void UBTService_CheckForPlayer::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds){
    AEnemyAI * EnemyPC = Cast<AEnemyAI>(OwnerComp.GetAIOwner());

    if (EnemyPC) {
        ACharacter *Enemy = Cast<ACharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());

        if (Enemy){
            // To get the blackboard component
            OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Object>(EnemyPC->EnemyKeyID, Enemy);
            UE_LOG(LogTemp, Warning, TEXT("I exist and can identify player."));
        }
        else{
            UE_LOG(LogTemp, Warning, TEXT("I exist but cant identify player."));

        }
    }
    else {
        UE_LOG(LogTemp, Warning, TEXT("Failed to find EnemyPC."));
    }
}