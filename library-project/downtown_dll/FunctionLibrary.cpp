#include "pch.h"

// String should be included before so that errors are not produced in header...
#include <string>
#include <map>
#include "FunctionLibrary.h"

#include <boost/foreach.hpp>
#include <boost/regex.hpp>
#include <boost/phoenix/phoenix.hpp>

#define ON_WINDOWS 1

#if ON_WINDOWS
#include <windows.h>
#endif

using namespace std;
// Required for functional programming
using namespace boost::phoenix::placeholders;
using boost::phoenix::val;
using boost::phoenix::construct;

/*------------------------------------------
	=> END SECTION : HEADERS AND NAMESPACE
	--------------------------------------*/

/*------------------------------------------
	=> START SECTION : FUNCTIONAL LIBRARY IMPLEMENTATION
--------------------------------------*/
struct RegexMatcher_Impl {

	typedef bool result_type;
	bool operator()(std::string in, std::string matchwith) const {
		boost::regex expr{ matchwith };
		return if_else(val(boost::regex_match(in, expr)), true, false)();
	}
};
// Returns : "true" if full string matches
boost::phoenix::function<RegexMatcher_Impl> RegexMatcher;

int sendStateChangeRequest(int initial, int final)
{
	States a = static_cast<States>(initial);
	States b = static_cast<States>(final);

	switch (a) {
	case Walking: {
		switch (b) {
		case Walking: { return 0; }
		case Grabbing: {break; }
		case Running: {
			WWdashAvailable = false;
			return (int)Sprint;
		}
		case Jumping: {
			return (int)Jump;
		}
		case Dashing: {
			if (WWdashAvailable == true) {
				WWdashAvailable = false;
				return (int)StartDashing;
			}
			else {
				return (int)DashNotAvailable;
			}
		}
		default: { return 0;  break; }
		}
	}
	case Grabbing:break;
	case Running:break;
	case Flying:break;
	case Jumping:break;
	case Falling:break;
	case Hanging:break;
	case Grounded:break;
	case Floating:break;
	default: { return 0; break; }
	}
	return 0;
}

void InitializeDLL()
{
	//Mapping boolean parameters(Used for routines to reduce unnecessary code)
	//references = new bool*[4];
	references[0] = WWdashAvailable;
	//*references[0] = &dashAvailable;
	references[1] = WWOnGround;
	references[2] = WWCanMoveMoveAhead;
	references[3] = WWCanMoveSideways;
	return;
}

void returnVectorList(WW::Vector3 in, WW::Vector3* &ass)
{
	//Vector3* ass;
	ass = new WW::Vector3[10];
	loop(i, 10) {
		ass[i].x = in.x * i;
		ass[i].y = in.y * i;
		ass[i].z = in.z * i;
	}
	return;
}

int WW_API testFunction(int myarray)
{
	int retval = 1;
	int myarr[5] = {1,2,3,4,5};


	//std::for_each(myarray.begin(), myarray.end(), retval*= 1);

	// Array iteration...
	BOOST_FOREACH(int& i, myarr) {
		retval *= i;
	}
	return retval;
	//boost::fusion::for_each(myarray.begin(), for_each.end(), retval *= _1);
	//return retval);
}

/*	
	Regex matches string. Parametrs: flag = 0
	
*/
bool WW_API RegexMatchSegment(std::string in, std::string with, int flag)
{
	//if_(RegexMatcher(in, with))[
	//	true
	//];
	
	switch (flag) {
	case -1: {
		boost::regex expr{ with };
		return boost::regex_match(in, expr);
		break;
	}
	case 0: {
		boost::regex expr{ "_" };
		boost::smatch what;
		boost::regex_search(in, what, expr);

		// See if user followed conventions...

		// Case 0 : Convention not followed, return regex search(bool)
		if (what.size() == 0) { 
			boost::regex expr2{ with };
			return boost::regex_match(in, expr2);
			//boost::
		}

		// Case 1: Convention followed. Strip type identifier and return match(bool)
		else if(what.size() == 1){
		}
	}
	}
	return false;
}

bool WW_API RegexTestFunction(std::string in, std::string with, int flag)
{
	return RegexMatcher(in, with)();
}

void WW_API UpdateStringMap(StrStrMap &in, string with)
{
	return;
}

void WW_API CopyFileFromTo(std::string from, std::string to, bool overwrite_flag)
{
	//CopyFile(LPCWSTR(from), to.c_str(), overwrite_flag);
	//LPCWSTR
	return;
}

void sendStateUpdate(int* array, int N)
{
	loop(i, N) {
		switch (array[i]) {
		case 0: references[i] = false; break;
		case 1: references[i] = true; break;
		case 2: break;
		default: break;
		}
	}
}
