#pragma once  

//shortens __declspec(dllexport) to DLL_EXPORT for windows. Ignores for linux.
#ifdef _WIN32
#define WW_API __declspec(dllexport)	
#else
#define WINTERWILDFIRE_API
#endif


using namespace std;
// Useful macros usable in any engine
#define loop(i,N) for(int i = 0; i < N; i++)

class FVector;

template<typename T>
class TArray;

//class string;
namespace WW {
	struct WW_API Vector3 {

		// Components
		float x;
		float y;
		float z;

	};

	union myunion {
		TArray<FVector> ue4VectorArray;
		vector<Vector3> myVectorArray;
	};
}





typedef std::map<string, string> StrStrMap;

/*
	@ Following are UE4 specific macros to make life easier for a C++ programmer.
	@ -- Actor initializations
	@ -- Mini algo macros like loops
*/
#define UE4_StaticActorInit PrimaryActorTick.bCanEverTick = false; \
							SceneRoot->SetMobility(EComponentMobility::Type::Static);




#ifdef __cplusplus		//if C++ is used convert it to C to prevent C++'s name mangling of method names
extern "C" {
#endif

	enum WW_API States {
		Walking,
		Grabbing,
		Running,
		Flying,

		Jumping,
		Falling,
		Hanging,
		Grounded,
		Floating,
		Dashing
	};
	enum WW_API Responses {
		DoNothing,
		Sprint,
		Jump,
		StartDashing,
		DashNotAvailable
	};

	//struct WINTERWILDFIRE_API Vector3 {
	//	float x;
	//	float y;
	//	float z;
	//};
	bool WW_API WWdashAvailable = false;
	bool WW_API WWOnGround = false;
	bool WW_API WWCanMoveMoveAhead = true;
	bool WW_API WWCanMoveSideways = true;
	bool WW_API references[4];
	//bool* DLL_EXPORT dragon[4];

	int WW_API sendStateChangeRequest(int initial, int final);
	void WW_API sendStateUpdate(int* array, int N);
	void WW_API InitializeDLL();

	void WW_API returnVectorList(WW::Vector3 in, WW::Vector3* &ass);

	//int WW_API testFunction(myunion myarray);

	

	/*
		Regex matches string. Parameters: flag = 0
		@ flag -1    :  Match string as is fully             =>                ** ignores nothing **
		@ flag 0    :  Match string as is fully             =>                ** compares T_ from T_YourTexture_01 **
		@ flag 1    :  Ignore type identifier               =>                ** ignores YourTexture from T_YourTexture_01 **
		@ flag 2    :  Ignores type identifier and name     =>                ** ignores _01 from T_YourTexture_01 ** (Index as string is auto formatted)
	*/
	bool WW_API RegexMatchSegment(string in, string with, int flag);
	bool WW_API RegexTestFunction(string in, string with, int flag);
	void WW_API UpdateStringMap(StrStrMap &in, string with);

	WW::Vector3 WW_API addVector(WW::Vector3 in, WW::Vector3 with);
	// String manipulation...

	// File I/O operations...
	void WW_API CopyFileFromTo(std::string from, std::string to, bool overwrite_flag);

#ifdef __cplusplus
}
#endif