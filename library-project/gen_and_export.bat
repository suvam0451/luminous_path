@echo off
color 0a
title Build and Export libraries
set msbuild="C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\MSBuild\Current\Bin\amd64\msbuild.exe"

rem Options : Release, Debug
set config=Release
rem Sets architecture. Options : x64, x86
set Platform=x64

rem Run the command !!!
%msbuild% -property:Configuration=%config% -property:Platform=%Platform%

pause